<?php  ?>

<!-- First Blog Post -->
<h2>
    <a href="post.php?p_id=<?php echo $post_id ?>&more=no"><?php echo $post_title; ?></a>
</h2>

<p class="lead">by <a href="author_post.php?author=<?php echo $post_author; ?>&p_id=<?php echo $post_id; ?>"><?php echo $post_author; ?></a></p>

<p><span class="glyphicon glyphicon-time"></span><?php  echo $post_date; ?></p>

<div>                            
    <a href="post.php?p_id=<?php echo $post_id ?>&more=no">
        <img class="img-responsive" src="images/<?php echo $post_image; ?>" alt="picture of post">
    </a>
</div
 

<!--Post contents -->
<p>
    <?php  
        if (!isset($_GET['more'])){
            $post_content = substr($post_content,0,200);
            echo $post_content;
        } else {            
            echo $post_content;
            } 
    ?>
</p><!--/Post contents -->


<!-- More button -->
<?php 
   if (!isset($_GET['more'])){
        if(isset($_GET['p_id'])){
            $post_id = escape($_GET['p_id']);}
         echo "<a class='btn btn-primary btn-sm' name='summary' href='post.php?p_id={$post_id}&more=no'>More</a>";
   }    
?><!-- More button -->
