<?php ob_start(); ?>
<?php  include "includes/db.php"; ?>
<?php "../admin/functions.php" ?>
<?php  include "includes/header.php"; ?>


<?php 

if($_SERVER['REQUEST_METHOD'] == 'POST'){

    $username      = trim($_POST['username']);
    $password      = trim($_POST['password']);
    $user_email    = trim($_POST['email']);

    $error = ['username' => '',
              'email'    => '',
              'password' => ''
             ];
// username validation
    if(strlen($username)< 4){
        $error['username'] = "username to short";
    }

    if($username == ''){
        $error['username'] = "please give a username";
    }

    if(username_exists($username)){
        $error['username'] = "username allready exists !";
    }

// email validation
    if($user_email == ''){
        $error['email'] = "email cannot be empty !";
    }
    
    if(email_exists($user_email)){
        $error['email'] = "email allready exists !, <a href='index.php'>Please Login</a>";
    }

// password validation
    if($password == ''){
        $error['password'] = "password canot be empty !";
    }

    foreach ($error as $key => $value) {

        if(empty($value)){
            unset($error[$key]);
        }
            
    } // foreach

    if(empty($error)){
        register_user($username,$user_email,$password);
        login_user($username,$password);
    }
}

?>



<div id="wrapper">

<!-- Navigation -->
<?php include "includes/navigation.php"; ?>

<div id="page-wrapper">

<!-- Registration Form -->
<div class="container-fluid">

    <section id="login">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="form-wrap">
                    
                    <h1>Register</h1>

                        <form role="form" action="registration.php" method="post" id="login-form" autocomplete="off">

                            <div class="form-group">
                                <label for="username" class="sr-only">username</label>
                                <input type="text" name="username" id="username" class="form-control" placeholder="Enter Desired Username" autocomplete="on" value="<?php echo isset($username
                                    ) ? $username : '' ?>">
                                <p class=""><?php echo isset($error['username']) ? $error['username'] : '' ?></p>
                            </div>
                             <div class="form-group">
                                <label for="email" class="sr-only">Email</label>
                                <input type="email" name="email" id="email" class="form-control" placeholder="somebody@example.com" autocomplete="on" value="<?php echo isset($user_email
                                    ) ? $user_email : '' ?>">
                                <p class="red"><?php echo isset($error['email']) ? $error['email'] : '' ?></p>
                            </div>
                             <div class="form-group">
                                <label for="password" class="sr-only">Password</label>
                                <input type="password" name="password" id="key" class="form-control" placeholder="Password">
                                <p class=""><?php echo isset($error['password']) ? $error['password'] : '' ?></p>
                            </div>
                    
                            <input type="submit" name="register" id="btn-login" class="btn btn-custom btn-lg btn-block" value="Register">
                        </form>
                     
                    </div>
                </div> <!-- / col-xs-12 -->
            </div> <!-- / Row -->
        </div> <!-- / Container -->
    </section>

</div><!-- / Registration Form -->

<?php include "includes/footer.php";?>

</div><!-- / Page-wrapper -->

</div><!-- / Wrapper -->