-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 18, 2018 at 04:20 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--
CREATE DATABASE IF NOT EXISTS `cms` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `cms`;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(33) NOT NULL,
  `cat_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_title`) VALUES
(1, 'Red'),
(2, 'Yellow'),
(6, 'Purple'),
(24, 'Blue'),
(25, 'Orange'),
(26, 'Green');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(3) NOT NULL,
  `comment_post_id` int(3) NOT NULL,
  `comment_author` varchar(255) NOT NULL,
  `comment_email` varchar(255) NOT NULL,
  `comment_content` text NOT NULL,
  `comment_status` varchar(255) NOT NULL,
  `comment_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `comment_post_id`, `comment_author`, `comment_email`, `comment_content`, `comment_status`, `comment_date`) VALUES
(5, 7, 'Space Invader', 'space@space.un', 'Grooovy', 'approved', '2018-01-26'),
(12, 2, 'Darth Blogius', 'Darth@Darkside.com', 'I find your lack of faith disturbing..', 'approved', '2018-01-29'),
(16, 2, 'Serena', 'phpfreak@php.com', 'Programmers Love PHP', 'approved', '2018-02-01');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(3) NOT NULL,
  `post_category_id` int(3) NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post_author` varchar(255) NOT NULL,
  `post_date` date NOT NULL,
  `post_image` text NOT NULL,
  `post_content` text NOT NULL,
  `post_tags` varchar(255) NOT NULL,
  `post_comment_count` int(11) NOT NULL,
  `post_status` varchar(255) NOT NULL DEFAULT 'draft',
  `post_views_counts` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `post_category_id`, `post_title`, `post_author`, `post_date`, `post_image`, `post_content`, `post_tags`, `post_comment_count`, `post_status`, `post_views_counts`) VALUES
(2, 24, 'Blue post', 'John Doe', '2018-03-09', 'Blue_post.png', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Adipiscing elit pellentesque habitant morbi tristique senectus et netus. Fringilla ut morbi tincidunt augue interdum velit euismod. Lectus sit amet est placerat in egestas. Aliquet nibh praesent tristique magna sit amet purus gravida quis. Dui ut ornare lectus sit amet est placerat in egestas. Curabitur vitae nunc sed velit dignissim sodales ut. At erat pellentesque adipiscing commodo elit at. Suspendisse faucibus interdum posuere lorem ipsum dolor sit. Non tellus orci ac auctor augue mauris. Viverra adipiscing at in tellus integer. Morbi blandit cursus risus at ultrices mi tempus imperdiet nulla. Pharetra magna ac placerat vestibulum. Malesuada fames ac turpis egestas maecenas pharetra. Tellus orci ac auctor augue mauris augue. Velit euismod in pellentesque massa placerat duis ultricies lacus. Fames ac turpis egestas integer eget aliquet nibh. Tristique et egestas quis ipsum suspendisse ultrices gravida. Augue eget arcu dictum varius. Nec tincidunt praesent semper feugiat. Nunc non blandit massa enim nec dui nunc mattis enim. Potenti nullam ac tortor vitae purus. Tellus orci ac auctor augue. Nibh venenatis cras sed felis. Erat imperdiet sed euismod nisi. Nisl nisi scelerisque eu ultrices vitae auctor eu augue ut. Mattis pellentesque id nibh tortor id aliquet lectus proin nibh. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel. Id semper risus in hendrerit gravida rutrum. Nunc sed blandit libero volutpat. Nisl rhoncus mattis rhoncus urna neque viverra. Mauris cursus mattis molestie a. Pellentesque nec nam aliquam sem et tortor consequat. Sed ullamcorper morbi tincidunt ornare. Adipiscing bibendum est ultricies integer quis auctor elit. Non tellus orci ac auctor. Vel pharetra vel turpis nunc eget lorem dolor sed viverra. Sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula. Erat velit scelerisque in dictum. A cras semper auctor neque vitae tempus quam pellentesque. Id faucibus nisl tincidunt eget nullam non nisi est. Quam quisque id diam vel quam elementum pulvinar etiam non. Pretium aenean pharetra magna ac placerat vestibulum lectus mauris. Commodo elit at imperdiet dui accumsan sit amet nulla. Arcu felis bibendum ut tristique et egestas quis. Enim sit amet venenatis urna cursus. Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis. Gravida neque convallis a cras. Eget magna fermentum iaculis eu non. Mollis nunc sed id semper risus. Elementum nibh tellus molestie nunc non blandit. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique. Ut lectus arcu bibendum at varius vel pharetra. Donec enim diam vulputate ut pharetra sit amet. Urna et pharetra pharetra massa massa ultricies mi quis. Varius quam quisque id diam vel. Magna ac placerat vestibulum lectus mauris ultrices eros. Dui sapien eget mi proin sed libero enim sed. Sed faucibus turpis in eu mi bibendum neque egestas congue. Commodo nulla facilisi nullam vehicula ipsum a arcu cursus. Rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar. Ornare quam viverra orci sagittis eu volutpat odio. Vitae et leo duis ut diam quam. Tortor dignissim convallis aenean et tortor at risus. Mattis nunc sed blandit libero volutpat. Sapien pellentesque habitant morbi tristique senectus et netus et. Varius vel pharetra vel turpis nunc eget lorem dolor sed. Massa enim nec dui nunc mattis enim ut tellus. Aliquam nulla facilisi cras fermentum odio eu feugiat pretium nibh.</p>', 'Blue, post, php', 2, 'Published', 2),
(6, 6, 'Purple post', 'H Smith', '2018-04-01', 'Purple post.png', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Adipiscing elit pellentesque habitant morbi tristique senectus et netus. Fringilla ut morbi tincidunt augue interdum velit euismod. Lectus sit amet est placerat in egestas. Aliquet nibh praesent tristique magna sit amet purus gravida quis. Dui ut ornare lectus sit amet est placerat in egestas. Curabitur vitae nunc sed velit dignissim sodales ut. At erat pellentesque adipiscing commodo elit at. Suspendisse faucibus interdum posuere lorem ipsum dolor sit. Non tellus orci ac auctor augue mauris. Viverra adipiscing at in tellus integer. Morbi blandit cursus risus at ultrices mi tempus imperdiet nulla. Pharetra magna ac placerat vestibulum. Malesuada fames ac turpis egestas maecenas pharetra. Tellus orci ac auctor augue mauris augue. Velit euismod in pellentesque massa placerat duis ultricies lacus. Fames ac turpis egestas integer eget aliquet nibh. Tristique et egestas quis ipsum suspendisse ultrices gravida. Augue eget arcu dictum varius. Nec tincidunt praesent semper feugiat. Nunc non blandit massa enim nec dui nunc mattis enim. Potenti nullam ac tortor vitae purus. Tellus orci ac auctor augue. Nibh venenatis cras sed felis. Erat imperdiet sed euismod nisi. Nisl nisi scelerisque eu ultrices vitae auctor eu augue ut. Mattis pellentesque id nibh tortor id aliquet lectus proin nibh. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel. Id semper risus in hendrerit gravida rutrum. Nunc sed blandit libero volutpat. Nisl rhoncus mattis rhoncus urna neque viverra. Mauris cursus mattis molestie a. Pellentesque nec nam aliquam sem et tortor consequat. Sed ullamcorper morbi tincidunt ornare. Adipiscing bibendum est ultricies integer quis auctor elit. Non tellus orci ac auctor. Vel pharetra vel turpis nunc eget lorem dolor sed viverra. Sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula. Erat velit scelerisque in dictum. A cras semper auctor neque vitae tempus quam pellentesque. Id faucibus nisl tincidunt eget nullam non nisi est. Quam quisque id diam vel quam elementum pulvinar etiam non. Pretium aenean pharetra magna ac placerat vestibulum lectus mauris. Commodo elit at imperdiet dui accumsan sit amet nulla. Arcu felis bibendum ut tristique et egestas quis. Enim sit amet venenatis urna cursus. Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis. Gravida neque convallis a cras. Eget magna fermentum iaculis eu non. Mollis nunc sed id semper risus. Elementum nibh tellus molestie nunc non blandit. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique. Ut lectus arcu bibendum at varius vel pharetra. Donec enim diam vulputate ut pharetra sit amet. Urna et pharetra pharetra massa massa ultricies mi quis. Varius quam quisque id diam vel. Magna ac placerat vestibulum lectus mauris ultrices eros. Dui sapien eget mi proin sed libero enim sed. Sed faucibus turpis in eu mi bibendum neque egestas congue. Commodo nulla facilisi nullam vehicula ipsum a arcu cursus. Rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar. Ornare quam viverra orci sagittis eu volutpat odio. Vitae et leo duis ut diam quam. Tortor dignissim convallis aenean et tortor at risus. Mattis nunc sed blandit libero volutpat. Sapien pellentesque habitant morbi tristique senectus et netus et. Varius vel pharetra vel turpis nunc eget lorem dolor sed. Massa enim nec dui nunc mattis enim ut tellus. Aliquam nulla facilisi cras fermentum odio eu feugiat pretium nibh.</p>', 'Purple, post, ', 4, 'Draft', 0),
(7, 26, 'Green Post', 'Joan Doe', '2018-04-01', 'Green_post.png', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Adipiscing elit pellentesque habitant morbi tristique senectus et netus. Fringilla ut morbi tincidunt augue interdum velit euismod. Lectus sit amet est placerat in egestas. Aliquet nibh praesent tristique magna sit amet purus gravida quis. Dui ut ornare lectus sit amet est placerat in egestas. Curabitur vitae nunc sed velit dignissim sodales ut. At erat pellentesque adipiscing commodo elit at. Suspendisse faucibus interdum posuere lorem ipsum dolor sit. Non tellus orci ac auctor augue mauris. Viverra adipiscing at in tellus integer. Morbi blandit cursus risus at ultrices mi tempus imperdiet nulla. Pharetra magna ac placerat vestibulum. Malesuada fames ac turpis egestas maecenas pharetra. Tellus orci ac auctor augue mauris augue. Velit euismod in pellentesque massa placerat duis ultricies lacus. Fames ac turpis egestas integer eget aliquet nibh. Tristique et egestas quis ipsum suspendisse ultrices gravida. Augue eget arcu dictum varius. Nec tincidunt praesent semper feugiat. Nunc non blandit massa enim nec dui nunc mattis enim. Potenti nullam ac tortor vitae purus. Tellus orci ac auctor augue. Nibh venenatis cras sed felis. Erat imperdiet sed euismod nisi. Nisl nisi scelerisque eu ultrices vitae auctor eu augue ut. Mattis pellentesque id nibh tortor id aliquet lectus proin nibh. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel. Id semper risus in hendrerit gravida rutrum. Nunc sed blandit libero volutpat. Nisl rhoncus mattis rhoncus urna neque viverra. Mauris cursus mattis molestie a. Pellentesque nec nam aliquam sem et tortor consequat. Sed ullamcorper morbi tincidunt ornare. Adipiscing bibendum est ultricies integer quis auctor elit. Non tellus orci ac auctor. Vel pharetra vel turpis nunc eget lorem dolor sed viverra. Sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula. Erat velit scelerisque in dictum. A cras semper auctor neque vitae tempus quam pellentesque. Id faucibus nisl tincidunt eget nullam non nisi est. Quam quisque id diam vel quam elementum pulvinar etiam non. Pretium aenean pharetra magna ac placerat vestibulum lectus mauris. Commodo elit at imperdiet dui accumsan sit amet nulla. Arcu felis bibendum ut tristique et egestas quis. Enim sit amet venenatis urna cursus. Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis. Gravida neque convallis a cras. Eget magna fermentum iaculis eu non. Mollis nunc sed id semper risus. Elementum nibh tellus molestie nunc non blandit. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique. Ut lectus arcu bibendum at varius vel pharetra. Donec enim diam vulputate ut pharetra sit amet. Urna et pharetra pharetra massa massa ultricies mi quis. Varius quam quisque id diam vel. Magna ac placerat vestibulum lectus mauris ultrices eros. Dui sapien eget mi proin sed libero enim sed. Sed faucibus turpis in eu mi bibendum neque egestas congue. Commodo nulla facilisi nullam vehicula ipsum a arcu cursus. Rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar. Ornare quam viverra orci sagittis eu volutpat odio. Vitae et leo duis ut diam quam. Tortor dignissim convallis aenean et tortor at risus. Mattis nunc sed blandit libero volutpat. Sapien pellentesque habitant morbi tristique senectus et netus et. Varius vel pharetra vel turpis nunc eget lorem dolor sed. Massa enim nec dui nunc mattis enim ut tellus. Aliquam nulla facilisi cras fermentum odio eu feugiat pretium nibh.</p>', 'Green, post, blog\\', 4, 'Published', 0),
(8, 2, 'Yellow post', 'Dr Watson', '2018-04-18', 'Yellow_post.png', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Adipiscing elit pellentesque habitant morbi tristique senectus et netus. Fringilla ut morbi tincidunt augue interdum velit euismod. Lectus sit amet est placerat in egestas. Aliquet nibh praesent tristique magna sit amet purus gravida quis. Dui ut ornare lectus sit amet est placerat in egestas. Curabitur vitae nunc sed velit dignissim sodales ut. At erat pellentesque adipiscing commodo elit at. Suspendisse faucibus interdum posuere lorem ipsum dolor sit. Non tellus orci ac auctor augue mauris. Viverra adipiscing at in tellus integer. Morbi blandit cursus risus at ultrices mi tempus imperdiet nulla. Pharetra magna ac placerat vestibulum. Malesuada fames ac turpis egestas maecenas pharetra. Tellus orci ac auctor augue mauris augue. Velit euismod in pellentesque massa placerat duis ultricies lacus. Fames ac turpis egestas integer eget aliquet nibh. Tristique et egestas quis ipsum suspendisse ultrices gravida. Augue eget arcu dictum varius. Nec tincidunt praesent semper feugiat. Nunc non blandit massa enim nec dui nunc mattis enim. Potenti nullam ac tortor vitae purus. Tellus orci ac auctor augue. Nibh venenatis cras sed felis. Erat imperdiet sed euismod nisi. Nisl nisi scelerisque eu ultrices vitae auctor eu augue ut. Mattis pellentesque id nibh tortor id aliquet lectus proin nibh. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel. Id semper risus in hendrerit gravida rutrum. Nunc sed blandit libero volutpat. Nisl rhoncus mattis rhoncus urna neque viverra. Mauris cursus mattis molestie a. Pellentesque nec nam aliquam sem et tortor consequat. Sed ullamcorper morbi tincidunt ornare. Adipiscing bibendum est ultricies integer quis auctor elit. Non tellus orci ac auctor. Vel pharetra vel turpis nunc eget lorem dolor sed viverra. Sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula. Erat velit scelerisque in dictum. A cras semper auctor neque vitae tempus quam pellentesque. Id faucibus nisl tincidunt eget nullam non nisi est. Quam quisque id diam vel quam elementum pulvinar etiam non. Pretium aenean pharetra magna ac placerat vestibulum lectus mauris. Commodo elit at imperdiet dui accumsan sit amet nulla. Arcu felis bibendum ut tristique et egestas quis. Enim sit amet venenatis urna cursus. Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis. Gravida neque convallis a cras. Eget magna fermentum iaculis eu non. Mollis nunc sed id semper risus. Elementum nibh tellus molestie nunc non blandit. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique. Ut lectus arcu bibendum at varius vel pharetra. Donec enim diam vulputate ut pharetra sit amet. Urna et pharetra pharetra massa massa ultricies mi quis. Varius quam quisque id diam vel. Magna ac placerat vestibulum lectus mauris ultrices eros. Dui sapien eget mi proin sed libero enim sed. Sed faucibus turpis in eu mi bibendum neque egestas congue. Commodo nulla facilisi nullam vehicula ipsum a arcu cursus. Rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar. Ornare quam viverra orci sagittis eu volutpat odio. Vitae et leo duis ut diam quam. Tortor dignissim convallis aenean et tortor at risus. Mattis nunc sed blandit libero volutpat. Sapien pellentesque habitant morbi tristique senectus et netus et. Varius vel pharetra vel turpis nunc eget lorem dolor sed. Massa enim nec dui nunc mattis enim ut tellus. Aliquam nulla facilisi cras fermentum odio eu feugiat pretium nibh.</p>', 'Yellow, post, sun', 4, 'Published', 0),
(9, 25, 'Orange Post', 'John Doe', '2018-03-06', 'Orange_post.png', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Adipiscing elit pellentesque habitant morbi tristique senectus et netus. Fringilla ut morbi tincidunt augue interdum velit euismod. Lectus sit amet est placerat in egestas. Aliquet nibh praesent tristique magna sit amet purus gravida quis. Dui ut ornare lectus sit amet est placerat in egestas. Curabitur vitae nunc sed velit dignissim sodales ut. At erat pellentesque adipiscing commodo elit at. Suspendisse faucibus interdum posuere lorem ipsum dolor sit. Non tellus orci ac auctor augue mauris. Viverra adipiscing at in tellus integer. Morbi blandit cursus risus at ultrices mi tempus imperdiet nulla. Pharetra magna ac placerat vestibulum. Malesuada fames ac turpis egestas maecenas pharetra. Tellus orci ac auctor augue mauris augue. Velit euismod in pellentesque massa placerat duis ultricies lacus. Fames ac turpis egestas integer eget aliquet nibh. Tristique et egestas quis ipsum suspendisse ultrices gravida. Augue eget arcu dictum varius. Nec tincidunt praesent semper feugiat. Nunc non blandit massa enim nec dui nunc mattis enim. Potenti nullam ac tortor vitae purus. Tellus orci ac auctor augue. Nibh venenatis cras sed felis. Erat imperdiet sed euismod nisi. Nisl nisi scelerisque eu ultrices vitae auctor eu augue ut. Mattis pellentesque id nibh tortor id aliquet lectus proin nibh. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel. Id semper risus in hendrerit gravida rutrum. Nunc sed blandit libero volutpat. Nisl rhoncus mattis rhoncus urna neque viverra. Mauris cursus mattis molestie a. Pellentesque nec nam aliquam sem et tortor consequat. Sed ullamcorper morbi tincidunt ornare. Adipiscing bibendum est ultricies integer quis auctor elit. Non tellus orci ac auctor. Vel pharetra vel turpis nunc eget lorem dolor sed viverra. Sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula. Erat velit scelerisque in dictum. A cras semper auctor neque vitae tempus quam pellentesque. Id faucibus nisl tincidunt eget nullam non nisi est. Quam quisque id diam vel quam elementum pulvinar etiam non. Pretium aenean pharetra magna ac placerat vestibulum lectus mauris. Commodo elit at imperdiet dui accumsan sit amet nulla. Arcu felis bibendum ut tristique et egestas quis. Enim sit amet venenatis urna cursus. Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis. Gravida neque convallis a cras. Eget magna fermentum iaculis eu non. Mollis nunc sed id semper risus. Elementum nibh tellus molestie nunc non blandit. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique. Ut lectus arcu bibendum at varius vel pharetra. Donec enim diam vulputate ut pharetra sit amet. Urna et pharetra pharetra massa massa ultricies mi quis. Varius quam quisque id diam vel. Magna ac placerat vestibulum lectus mauris ultrices eros. Dui sapien eget mi proin sed libero enim sed. Sed faucibus turpis in eu mi bibendum neque egestas congue. Commodo nulla facilisi nullam vehicula ipsum a arcu cursus. Rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar. Ornare quam viverra orci sagittis eu volutpat odio. Vitae et leo duis ut diam quam. Tortor dignissim convallis aenean et tortor at risus. Mattis nunc sed blandit libero volutpat. Sapien pellentesque habitant morbi tristique senectus et netus et. Varius vel pharetra vel turpis nunc eget lorem dolor sed. Massa enim nec dui nunc mattis enim ut tellus. Aliquam nulla facilisi cras fermentum odio eu feugiat pretium nibh.</p>', 'orange,post, ', 4, 'Published', 0),
(65, 1, 'Red  Post', 'Joan Doe', '2018-04-17', 'Red_post.png', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Adipiscing elit pellentesque habitant morbi tristique senectus et netus. Fringilla ut morbi tincidunt augue interdum velit euismod. Lectus sit amet est placerat in egestas. Aliquet nibh praesent tristique magna sit amet purus gravida quis. Dui ut ornare lectus sit amet est placerat in egestas. Curabitur vitae nunc sed velit dignissim sodales ut. At erat pellentesque adipiscing commodo elit at. Suspendisse faucibus interdum posuere lorem ipsum dolor sit. Non tellus orci ac auctor augue mauris. Viverra adipiscing at in tellus integer. Morbi blandit cursus risus at ultrices mi tempus imperdiet nulla. Pharetra magna ac placerat vestibulum. Malesuada fames ac turpis egestas maecenas pharetra. Tellus orci ac auctor augue mauris augue. Velit euismod in pellentesque massa placerat duis ultricies lacus. Fames ac turpis egestas integer eget aliquet nibh. Tristique et egestas quis ipsum suspendisse ultrices gravida. Augue eget arcu dictum varius. Nec tincidunt praesent semper feugiat. Nunc non blandit massa enim nec dui nunc mattis enim. Potenti nullam ac tortor vitae purus. Tellus orci ac auctor augue. Nibh venenatis cras sed felis. Erat imperdiet sed euismod nisi. Nisl nisi scelerisque eu ultrices vitae auctor eu augue ut. Mattis pellentesque id nibh tortor id aliquet lectus proin nibh. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel. Id semper risus in hendrerit gravida rutrum. Nunc sed blandit libero volutpat. Nisl rhoncus mattis rhoncus urna neque viverra. Mauris cursus mattis molestie a. Pellentesque nec nam aliquam sem et tortor consequat. Sed ullamcorper morbi tincidunt ornare. Adipiscing bibendum est ultricies integer quis auctor elit. Non tellus orci ac auctor. Vel pharetra vel turpis nunc eget lorem dolor sed viverra. Sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula. Erat velit scelerisque in dictum. A cras semper auctor neque vitae tempus quam pellentesque. Id faucibus nisl tincidunt eget nullam non nisi est. Quam quisque id diam vel quam elementum pulvinar etiam non. Pretium aenean pharetra magna ac placerat vestibulum lectus mauris. Commodo elit at imperdiet dui accumsan sit amet nulla. Arcu felis bibendum ut tristique et egestas quis. Enim sit amet venenatis urna cursus. Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis. Gravida neque convallis a cras. Eget magna fermentum iaculis eu non. Mollis nunc sed id semper risus. Elementum nibh tellus molestie nunc non blandit. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique. Ut lectus arcu bibendum at varius vel pharetra. Donec enim diam vulputate ut pharetra sit amet. Urna et pharetra pharetra massa massa ultricies mi quis. Varius quam quisque id diam vel. Magna ac placerat vestibulum lectus mauris ultrices eros. Dui sapien eget mi proin sed libero enim sed. Sed faucibus turpis in eu mi bibendum neque egestas congue. Commodo nulla facilisi nullam vehicula ipsum a arcu cursus. Rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar. Ornare quam viverra orci sagittis eu volutpat odio. Vitae et leo duis ut diam quam. Tortor dignissim convallis aenean et tortor at risus. Mattis nunc sed blandit libero volutpat. Sapien pellentesque habitant morbi tristique senectus et netus et. Varius vel pharetra vel turpis nunc eget lorem dolor sed. Massa enim nec dui nunc mattis enim ut tellus. Aliquam nulla facilisi cras fermentum odio eu feugiat pretium nibh.</p>', 'Red, post, version 2', -2, 'Published', 0),
(67, 26, 'one more Green ', 'Administrator Superior', '2018-03-07', 'Green_post.png', '<p>lorem ipsum blipsum</p>', 'green, post, superior', 0, 'Draft', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(3) NOT NULL,
  `username` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_firstname` varchar(255) NOT NULL,
  `user_lastname` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_image` text NOT NULL,
  `user_role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `user_password`, `user_firstname`, `user_lastname`, `user_email`, `user_image`, `user_role`) VALUES
(5, 'Admin', '$2y$12$Ldr4Ul1aax4un3TQ87Ne/u8Zd7XYZINdImyi6RbCOyc0WUhOMWuA2', 'Administrator', 'Superior', 'admin@mail.com', 'user-icon-placeholder.png', 'Admin'),
(9, 'User', '$2y$12$Ox7zrIRhvqYL0EzYxLDsFeke1AhNXbo9qJCu3YTf5AJ7AW90GpiRO', 'User', 'Simple', 'user@mail.com', 'user-icon-placeholder.png', 'Subscriber'),
(10, 'arianna_s', '$2y$12$nw0iQSh4ULlsmQjMDEhEl.xZXxPkyCBMmgqCI2yP1Ai9EFCsLwLnu', 'Arianna', 'Salili', 'arianna@maths.com', 'user-icon-placeholder.png', 'subscriber'),
(11, 'daman', '$2y$12$9iZEPzqi.zuJXK1ZDRmj1eP74ZGBmEy/z13hhTpINvetAlwqi7L/K', 'Da', 'Man', 'daman@ret', 'user-icon-placeholder.png', 'Subscriber'),
(12, 'testing  one more', '$2y$12$0.Vz8NI99pDSPU.pZOkxXu/bVPOWfsXFC4gZ2NrqYySGs3MgTFDjS', '', '', 'datest@grip', 'user-icon-placeholder.png', 'Subscriber'),
(13, 'daman', '$2y$12$4kXywqslwO1/fK892wDguuacHM20JOnIGzqZF.WrnIQnaOX2TxnD2', '', '', 'dsf@ss', 'user-icon-placeholder.png', 'Subscriber');

-- --------------------------------------------------------

--
-- Table structure for table `users_online`
--

CREATE TABLE `users_online` (
  `id` int(11) NOT NULL,
  `session` varchar(255) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_online`
--

INSERT INTO `users_online` (`id`, `session`, `time`) VALUES
(3, 'aa3f8274e905a50c1d38f43daabada99', 1519047711),
(4, 'fea3ab1d8115cce27a06df13cb742595', 1518774553);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `role_id` int(3) NOT NULL,
  `role_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`role_id`, `role_type`) VALUES
(1, 'admin'),
(2, 'user'),
(3, 'guest');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users_online`
--
ALTER TABLE `users_online`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(33) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users_online`
--
ALTER TABLE `users_online`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `role_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
