<?php ob_start(); ?>
<?php  include "includes/db.php"; ?>
<?php  include "includes/header.php"; ?>


<?php 

if(isset($_POST['send'])){
          
    $to      = "kon.xen@gmail.com.com";
    $subject = wordwrap($_POST['subject'], 70);
    $body    = escape($_POST['body']);
    $header  = "From:" . escape($_POST['email']);

    mail($to,$subject,$body,$header);
}

?>

<div id="wrapper">

<!-- Navigation -->
<?php  include "includes/navigation.php"; ?>

    <div id="page-wrapper">

<!-- Page Content -->
<div class="container">

    <section id="login">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="form-wrap">
                    
                    <h1>Contact</h1>

                        <form role="form" action="contact.php" method="post" id="login-form" autocomplete="off">

                            <div class="form-group">
                                <label for="email" class="sr-only">Email</label>
                                <input type="email" name="email" id="email" class="form-control" placeholder="Your E-mail">
                            </div>
                            
                            <div class="form-group">
                                <label for="subject" class="sr-only">Subject</label>
                                <input type="text" name="subject" id="email" class="form-control" placeholder="Subject">
                            </div>

                            <div class="form-group">                                
                                <textarea class="form-control" name="body" id="" cols="50" rows="10"></textarea>
                            </div>
                    
                            <input type="submit" name="send" id="btn-send" class="btn btn-custom btn-lg btn-block" value="Send">
                        </form>
                     
                    </div>
                </div> <!-- /.col-xs-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section>


<?php include "includes/footer.php";?>

<div><!-- / page-wrapper -->

</div> <!-- / Wrapper -->
