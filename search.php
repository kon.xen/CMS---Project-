<?php include "includes/db.php"; ?>
<?php include "includes/header.php"; ?>
<?php require_once "admin/includes/functions.php"; ?>



<!-- Navigation -->
<?php include "includes/navigation.php";?>
   
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Search Column -->
            <div class="col-md-8">

                <?php

                    if (isset($_POST['submit'])){

                        $search = $_POST['search'];


                        // $search_query = mysqli_prepare($connection,"SELECT post_id, post_title, post_author, post_date, post_image, post_content FROM posts WHERE post_tags LIKE ?");
                        // mysqli_stmt_bind_param($search_query,"s",$search);
                        // mysqli_stmt_execute($search_query);
                        // mysqli_stmt_bind_result($search_query, $post_id, $post_title, $post_author, $post_date, $post_image, $post_content);
                        // mysqli_stmt_fetch($search_query);                        
                        // mysqli_stmt_close($search_query);


                        $query = "SELECT * FROM posts WHERE post_tags LIKE '%$search%'";
                        $search_query = mysqli_query($connection, $query); 

                        if(!$search_query){
                            die("QUERY FAILED" . mysqli_error($connection));
                         }

                        $count = mysqli_num_rows($search_query);
                        // $count = mysqli_num_rows($search_query);
                                                            
                        if($count == 0) {

                        echo"<h1>NO RESULT</h1>";

                    } else {

                        while ($row = mysqli_fetch_assoc($search_query)) {
                            $post_id      = $row['post_id'];
                            $post_title   = $row['post_title'];
                            $post_author  = $row['post_author'];
                            $post_date    = $row['post_date'];
                            $post_image   = $row['post_image'];
                        $post_content = substr($row['post_content'],0,200);
                        
                ?>

                        <!-- Blog Post display -->
                        <?php include "blogpost.php";?>


                <?php  } }} ?>

                             
            </div><!-- Blog Search-->
            
        <?php include "includes/sidebar.php"; ?>

    </div> <!-- /.row -->

</div>><!-- / Container-->     
       

<?php include "includes/footer.php"; ?>
