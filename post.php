<?php include "includes/db.php"; ?>
<?php include "includes/header.php"; ?>


<!-- Navigation -->
<?php include "includes/navigation.php";?>
   
<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">
            <?php 
            
                if(isset($_GET['p_id'])){

                    $the_post_id = escape($_GET['p_id']);

                    //post views count ...
                    $stmnt1 = mysqli_prepare($connection,"UPDATE posts SET post_views_counts = post_views_counts + 1 WHERE post_id = ?");

                    //Pull out post information...
                    $stmnt2 = mysqli_prepare($connection,"SELECT post_title, post_author, post_date, post_image, post_content FROM posts WHERE post_id = ?");


                    //post views count ...
                    mysqli_stmt_bind_param($stmnt1,"i",$the_post_id );
                    mysqli_stmt_execute($stmnt1);
                    mysqli_stmt_close($stmnt1);

                   
                    //Pull out post information... 
                    mysqli_stmt_bind_param($stmnt2,"i",$the_post_id );
                    mysqli_stmt_execute($stmnt2);
                    mysqli_stmt_bind_result($stmnt2,$post_title, $post_author, $post_date, $post_image, $post_content);

                    mysqli_stmt_fetch($stmnt2);
                    mysqli_stmt_close($stmnt2);
            ?>

            <?php include "blogpost.php";?>

                    
            <?php } 

                 else {
                
                    header("Location: index.php");
                }
            ?>



            <!-- NEW Blog Comments DB operations-->
            <?php 

            //collect fields to enter to table
            if(isset($_POST['create_comment'])){ 
                $comment_author  = escape($_POST['comment_author']);
                $comment_email   = escape($_POST['comment_email']);
                $comment_content = escape($_POST['comment_content']);

                if(!empty($comment_author) && !empty($comment_content) && !empty($comment_email)){

                    
                    $query = "INSERT INTO comments (comment_post_id, comment_author, comment_email, comment_content, comment_status, comment_date ) ";

                    $query .= "VALUES ($the_post_id, '{$comment_author}', '{$comment_email}', '{$comment_content}', 'pending', now()) ";

                    $create_comment_query = mysqli_query($connection,$query);

                    if(!$create_comment_query){
                        die ('QUERY FAILED' . mysqli_error($connection));
                    }

                    //Comment count ...
                    $stmnt3 = mysqli_prepare($connection,"UPDATE posts SET post_comment_count = post_comment_count + 1 WHERE post_id = ?");

                    mysqli_stmt_bind_param($stmnt3,"i",$the_post_id );
                    mysqli_stmt_execute($stmnt3);
                    mysqli_stmt_close($stmnt3);
                   
                } else {
                    echo "<script>alert('Fields cannot be empty')</script>";
                    }
                
            } 

            ?><!-- / NEW Blog Comments DB operations-->

            <!-- New Comment Form -->
            <div class="well">
                <h4>Leave a Comment:</h4>
                <form action="" method="post" role="form">

                     <div class="form-group">
                        <label for="comment_author">Name</label>
                        <input class="form-control" type="text" name="comment_author">
                    </div>

                    <div class="form-group">
                        <label for="comment_email">E-mail</label>
                        <input class="form-control" type="text" name="comment_email">
                    </div>

                    <div class="form-group">
                        <label for="comment_content">Comment</label>
                        <textarea name="comment_content" class="form-control" rows="3"></textarea>
                    </div>

                    <button type="submit" name="create_comment" class="btn btn-primary">Submit</button>
                </form>
            </div><!-- / New Comment Form -->

                <hr>

                <!-- Posted Comments DB Read & Display-->
                <?php

                $query  = "SELECT * FROM comments WHERE comment_post_id = {$the_post_id} AND comment_status = 'approved' ORDER BY comment_id DESC";

                $select_comment_query = mysqli_query($connection, $query);
               

                if(!$select_comment_query) {
                    die('Query failed !! yo ' . mysqli_error($connection));
                }

              
                while ($row = mysqli_fetch_array($select_comment_query)) {
                    $comment_date = $row['comment_date'];
                    $comment_content = $row['comment_content']; 
                    $comment_author = $row['comment_author'];
                ?> <!-- Posted Comments DB retrieval-->

            <!-- Comment Layout-->
            <div class="media">
                <a class="pull-left" href="#">
                    <img class="media-object" src="http://placehold.it/64x64" alt="commenter's image">
                </a>
                <div class="media-body">
                    <h4 class="media-heading"><?php echo $comment_author; ?>
                        <small><?php echo $comment_date; ?></small>
                    </h4>
                    <?php echo $comment_content; ?>
                </div>
            </div><!-- / Comment Layout-->  

                <?php } ?> <!-- / Posted Comments DB Read & Display-->  


        </div><!-- / Blog Entries Column -->

        <!-- Blog Sidebar Widgets Column -->
        <?php include "includes/sidebar.php" ?>

    </div><!-- /.row -->

</div><!-- / Container -->
       

        


<?php include "includes/footer.php" ?>
