<?php include_once "includes/db.php"; ?>
<?php include_once "includes/header.php"; ?>


    <!-- Navigation -->
<?php include "includes/navigation.php";?>
   
    <!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries -->
        <div class="col-md-8">
            <?php           

            if (isset($_GET['category'])){
                
                $post_category_id = escape($_GET['category']);

                if (is_admin($_SESSION['username'])){

                 // Preparing the statements (queries)
                $stmnt_1 = mysqli_prepare($connection,"SELECT post_id, post_title, post_author, post_date, post_image, post_content FROM posts WHERE post_category_id = ?");

                                
                } else {

                $stmnt_2 = mysqli_prepare($connection,"SELECT post_id, post_title, post_author, post_date, post_image, post_content FROM posts WHERE post_category_id = ? AND post_status = ?");

                $published = 'published';
                
                }


                if(isset($stmnt_1)){
                    // executing statement 1. needs also "mysqli_stmt_result" to bind the output together. because is a read query.
                    mysqli_stmt_bind_param($stmnt_1,"i",$post_category_id);
                    mysqli_stmt_execute($stmnt_1);
                    mysqli_stmt_bind_result($stmnt_1, $post_id, $post_title, $post_author, $post_date, $post_image, $post_content);

                    

                    $stmnt = $stmnt_1;
                    

                } else {
                    // executing statement 2. needs also "mysqli_stmt_result" to bind the output together. because is a read query.
                    mysqli_stmt_bind_param($stmnt_2, "is", $post_category_id, $published);
                    mysqli_stmt_execute($stmnt_2);
                    mysqli_stmt_bind_result($stmnt_2, $post_id, $post_title, $post_author, $post_date, $post_image, $post_content);


                    $stmnt = $stmnt_2;
                    
                }

                // !!! does not work if I close the statements... also the num_rows doesn't work atll witht the statements....!!!


                //mysqli_stmt_close($stmnt_1);

                //mysqli_stmt_close($stmnt_2);


                // if (mysqli_stmt_num_rows($stmnt) === 0){

                //     echo "<h1 class = 'text-center'>No categories available</h1>";
                // } 

                while (mysqli_stmt_fetch($stmnt)){
                  
                    $post_content = substr($post_content,0,200); 
                    include "blogpost.php";

                } 

                 mysqli_stmt_close($stmnt); 


            } else { 

                    header("location: index.php"); 

                }                

            ?>



        </div> <!-- / Blog Entries -->

        <!-- Blog Sidebar Widgets Column -->
        <?php include "includes/sidebar.php"; ?>

    </div><!-- /.row -->
        
</div><!-- / Container-->
        

<?php include "includes/footer.php"; ?>
