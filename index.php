<?php ob_start(); ?>
<?php include "includes/db.php"; ?>
<?php include "includes/header.php"; ?>


<div id="wrapper">

<!-- Navigation -->
<?php include "includes/navigation.php";?>

    <div id="page-wrapper">
   
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <!-- Blogs -->
            <div class="col-md-8">

                <!-- Retrieve Post data-->                
                <?php 

                pagination_calculation('posts','post_status',"'published'");                  

              
                // select that many posts to show / page

               
                    $select_all_posts_query = mysqli_prepare($connection, "SELECT post_id, post_title, post_author, post_date, post_image, post_content, post_status FROM posts WHERE post_status = 'Published' LIMIT ?, 5");

                    mysqli_stmt_bind_param($select_all_posts_query,"i", $page_1);
                    mysqli_stmt_execute($select_all_posts_query);
                    mysqli_stmt_bind_result($select_all_posts_query,$post_id, $post_title, $post_author, $post_date, $post_image, $post_content, $post_status);

                    while ($row = mysqli_stmt_fetch($select_all_posts_query)) {
                       
                        if($post_status == 'Published') {
                                                 
                ?><!-- / Retrieve Post data-->

                <!-- Blog Post layout -->
                <?php include "blogpost.php";?>

                <?php } }
                
                mysqli_stmt_close($select_all_posts_query);   ?>

                
            </div><!-- / Blogs -->


            <!-- Blog Sidebar Widgets Column -->
            <?php include "includes/sidebar.php"; ?>


        </div><!-- / row -->

        
        <!-- Pagination layout -->            
        <?php include "includes/pagination.php"; ?>
      

    </div><!--/ Container --> 
                   



 </div><!-- / Page-Wrapper -->

   <?php include "includes/footer.php"; ?>


</div> <!-- / Wrapper -->

