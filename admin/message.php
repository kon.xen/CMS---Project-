<?php include "includes/admin_header.php"; ?>

<?php if(!is_admin($_SESSION['username'])){

    header("location: index.php");
}
?>

<?php $message = file_get_contents("../message.txt");?>

<div id="wrapper">
    
    <?php include "includes/admin_navigation.php"; ?>

    <div id="page-wrapper">

        <div class="container-fluid">
            
            <div class="row"><!-- Page Heading -->
                
                <div class="col-lg-12">

                    <h3 class="page-header">Message | <small><?php echo $_SESSION['username']; ?></small></h3>

                    <div class="container-fluid">
						<form action="" method="post" enctype="multipart/form-data"><!-- Add Post form -->
							<div class="form-group">
								<label for="post_content">Message</label>
								<textarea class="form-control" name="post_content" id="body" cols="30" rows="10"><?php echo $message; ?></textarea>
							</div>

							<div class="form-group">
								<input class="btn btn-primary" type="submit" name="publish" value="Publish">
							</div>
						</form><!-- Message form -->  
					</div>

                </div><!--  / -->                     
                    
            </div><!-- /.row --> 

        </div><!-- /.container-fluid -->

        <?php include "../includes/footer.php"; ?>             

    </div><!-- / Page Wraper -->

</div><!-- / Wraper -->

       
<?php 
	if(isset($_POST['publish'])){

		$filename = "../message.txt";
		$message = $_POST['post_content'];

	    file_put_contents ($filename,$message);
	}		
		
 ?>