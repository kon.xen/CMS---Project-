<?php include "includes/admin_header.php"; ?>
<?php include "includes/profile_queries.php"; ?>

<div id="wrapper">
    
    <?php include "includes/admin_navigation.php"; ?>

        <div id="page-wrapper">

            <div class="container-fluid">
                
                <div class="row">
                    
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome to Admin 
                            <small>Author</small>
                        </h1>
                        
                        <!-- Update Post form -->
                        <form action="" method="post" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="username">username</label>
                                <input class="form-control" type="text" name="username" value="<?php echo $username; ?>">
                            </div>
                            
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input class="form-control" type="password" name="user_password" value="<?php echo $user_password; ?>">
                            </div>

                            <div class="form-group">
                                <label for="first_name">Name</label>
                                <input class="form-control" type="text" name="user_firstname" value="<?php echo $user_firstname; ?>">
                            </div>

                            <div class="form-group">
                                <label for="last_name">Surname</label>
                                <input class="form-control" type="text" name="user_lastname" value="<?php echo $user_lastname; ?>">
                            </div>

                            <div class="form-group">
                                <label for="email">e-mail</label>
                                <input class="form-control" type="email" name="user_email" value="<?php echo $user_email; ?>">
                            </div>
                            
                            <div class="form-group">
                                <img src="../images/<?php echo $user_image; ?>" width='100' alt="">
                                <input type="file" name="image">
                            </div>  
                            
                            <div class="form-group">
                                <select name="user_role" id="">
                                    <?php echo "<option value='$user_role'>$user_role</option>"; ?>  
                                    <option value='Admin'>Admin</option>
                                    <option value='Subscriber'>Subscriber</option>
                                    <option value='Guest'>Guest</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <input class="btn btn-warning" type="submit" name="update_profile" value="Update profile">
                            </div>

                        </form><!-- / Update Post form --> 

                        
                    </div><!-- / Column-->  
                        
                </div><!-- / Row -->    

            </div><!-- / Container-fluid -->    

        <?php include "../includes/footer.php"; ?>
        
        </div><!-- / Page wraper -->

    </div><!-- / Wraper -->



       
