<?php include "includes/admin_header.php"; ?>

<div id="wrapper">
    
    <?php include "includes/admin_navigation.php"; ?>

        <div id="page-wrapper">

            <div class="container-fluid">
                
                <div class="row"><!-- Page Heading -->
                    
                    <div class="col-lg-12">

                        <h3 class="page-header">Posts | <small><?php echo $_SESSION['username']; ?></small></h3>

                        <!-- Loader --> 
                        <?php 

                        if(isset($_GET['source'])){
                                $source = (escape($_GET['source']));

                                
                        } else{
                            $source ='all_post';
                        }

                        switch ($source) {
                            case 'add_post':
                                
                                include "includes/add_post.php";
                                break;

                            case 'edit_post':

                                include "includes/edit_post.php";
                                break;
                                
                            case '200':
                                echo "AWESOME!";
                                break;

                            case 'all_post':

                                if(!is_admin($_SESSION['username'])){
                                    include "includes/view_user_posts.php";

                                } else {
                                    include "includes/view_all_posts.php";
                                    break;
                                }
                    }   

                        ?><!-- / Loader --> 
                                               
                    </div><!--  / -->                   
                        
                </div><!-- /.row --> 

            </div><!-- /.container-fluid -->

            <?php include "../includes/footer.php"; ?>             

        </div><!-- / Page Wraper -->

</div><!-- / Wraper -->       
