<?php $author = $_SESSION['firstname']." ".$_SESSION['lastname']; ?>
<!-- Top Widgets -->

                <div class="row">

                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-file-text fa-5x"></i>
                                    </div>

                                    <div class="col-xs-9 text-right">
                                        <div class='huge'><?php echo $post_count = item_count_plus('posts','post_author',var_export($author,true)); ?></div>               
                                        <div>Posts</div>
                                    </div>

                                </div>
                            </div>
                            <a href="posts.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-comments fa-5x"></i>
                                    </div>                                    
                                  
                                    <div class="col-xs-9 text-right">
                                      <div class='huge'><?php echo $comment_count = item_count_comments('posts','post_author',var_export($author,true)); ?></div>
                                      <div>Comments</div>
                                    </div>

                                </div>
                            </div>
                            <a href="comments_user.php">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>


                </div><!-- /.row -->
