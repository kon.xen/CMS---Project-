<?php //include "includes/admin_header.php"; ?>
<?php include "admin_tools.php"; ?>

 
<!--  ALL POSTS FORM -->
<form action="" method="post">

    <!--  Admin tools controls -->
    <table class="table table-condensed stable-hover">

        <!-- rolldown -->
        <div id="bulkOptionsContainer" class="col-xs-4">
    
            <select class="form-control" name="bulk_options" id="">
                <option value="">Admin tools</option>
                <option value="Published">Publish</option>
                <option value="Draft">Draft</option>
                <option value="clone">Clone</option>
                <option value="delete">Delete</option> 
                <option value="reset">Views Reset</option>  
            </select>
    
        </div><!-- / rolldown -->

        <!-- buttons -->
        <div class="col-xs-4">
            <input type="submit" name="submit" class="btn btn-success" value="Apply">
            <a href="posts.php?source=add_post" value="add_post" class="btn btn-primary">Add New</a>
        </div><!-- buttons -->
    
    </table><!--  / Admin tools controls -->


    <!-- All posts Table -->
    <table class="table table-condensed table-hover">

        <thead>
            <tr>
                <th><input id="selectAllBoxes" type="checkbox"></th>
                <th>ID</th>
                <th>Author</th>
                <th>Title</th>
                <th>Category</th>
                <th>Status</th>
                <th>Views</th>
                <th>Image</th>
                <th>Tags</th>
                <th>No.</th>
                <th>Date</th>
                <th>View</th>
                <th>Edit</th>
                <th>Delete</th>                
            </tr>
        </thead>

        <tbody>

        <!-- Rows generator -->
        <?php

        //Connecting two tables - posts & categories
        $query = "SELECT posts.post_id, posts.post_content, posts.post_author, posts.post_title, posts.post_category_id, posts.post_status, posts.post_image, posts.post_tags, posts.post_comment_count, posts.post_date, posts.post_views_counts,categories.cat_id, categories.cat_title FROM posts LEFT JOIN categories ON posts.post_category_id = categories.cat_id ORDER BY posts.post_id DESC";

        // loading values from table &...
        $select_posts = mysqli_query($connection,$query);

        while ($row = mysqli_fetch_assoc($select_posts)) {
            $post_id        = $row['post_id'];
            $post_cat_id    = $row['post_category_id'];
            $post_title     = $row['post_title'];
            $post_author    = $row['post_author'];
            $post_date      = $row['post_date'];
            $post_image     = $row['post_image'];
            $post_content   = $row['post_content'];
            $post_tags      = $row['post_tags'];
            $post_comCount  = $row['post_comment_count'];
            $post_status    = $row['post_status']; 
            $post_Vcount    = $row['post_views_counts'];  
            $cat_title      = $row['cat_title'];       
            $cat_id         = $row['cat_id'];   
        
        // .. columns.. 
        echo "<tr>";
            ?>

            <!-- Checkboxes -->
            <td><input class="checkboxes" type="checkbox" name="checkBoxArray[]" value="<?php echo $post_id; ?>"></td><!-- /Checkboxes -->

            <?php

            // ---------  Author Column..
            echo "<td>{$post_id}</td>";

            if (isset($post_author) || !empty($post_author)){
                echo "<td>{$post_author}</td>";

            } elseif(isset($post_user) || !empty($post_user)) {
                 echo "<td>{$post_user}</td>";

            }

            // ********** Categories Column..
            echo "<td>{$post_title}</td>";

            echo "<td>{$cat_title}</td>";

            // **********  Status Column..
            echo "<td>{$post_status}</td>";

            // **********  Views Column..
            echo "<td>$post_Vcount</td>";

            // **********  Image Column..
            echo "<td><img width='100' src='../images/$post_image' alt='image'></td>";

            // **********  Tags Column..
            echo "<td>{$post_tags}</td>";

            // **********  Coments Column..
            echo "<td>{$post_comCount}</td>";

            // **********  Date Column..
            echo "<td>{$post_date}</td>";

            // **********  View Column ** 
            echo "<td><a class='btn btn-sm btn-info' href='../post.php?p_id={$post_id}'> View</a></td>";

             // **********  Edit Column ** 
            echo "<td><a class='btn btn-sm btn-primary' href='posts.php?source=edit_post&p_id={$post_id}'>Edit</a></td>";
                    
            ?>       
            <!-- **********  Delete Column.. -->
            <form method="post" action="">
                <input type="hidden" name="post_id" value="<?php echo $post_id ?>" >
                <td><input class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Are you sure?!');" name="delete" value="Delete"></td>
            </form>
            <?php  } ?> <!-- / Columns -->
      
        </tbody>

    </table><!-- / All posts Table -->

</form><!-- / ALL POSTS FORM -->

<!-- Delete post And related comments !-->
<?php 

if(isset($_POST['delete'])){

    $the_post_id = ($_POST['post_id']);
    delete_object('posts','post_id', $the_post_id);
    delete_object('comments','comment_post_id', $the_post_id);
    header(("location: posts.php"));
}
  
?>   

