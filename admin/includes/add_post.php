<?php 	

	if(isset($_POST['create_post'])){

		$post_category_id = escape($_POST['post_category']);
		$post_title = escape($_POST['title']);
		$post_author = $_SESSION['firstname']." ".$_SESSION['lastname'];
		$post_date = date('d-m-y');
		$post_status = escape($_POST['post_status']);
		$post_image = $_FILES['image'] ['name'];
		$post_image_temp = $_FILES['image'] ['tmp_name'];
		$post_tags = escape($_POST['post_tags']);
		$post_content = escape($_POST['post_content']);	
				
		move_uploaded_file($post_image_temp, "../images/$post_image");

		$create_post_query = mysqli_prepare($connection,"INSERT INTO posts(post_category_id, post_title, post_author, post_date, post_image, post_content, post_tags, post_status ) VALUES (?, ?, ?, now(), ?, ?, ?, ?)");
            
        mysqli_stmt_bind_param($create_post_query,"issssss", $post_category_id, $post_title, $post_author, $post_image, $post_content, $post_tags, $post_status);
        mysqli_stmt_execute($create_post_query);
        mysqli_stmt_close($create_post_query);


		$the_post_id = mysqli_insert_id($connection);


		echo "<p class='bg-success'>Post Created. <a href='../post.php?p_id={$the_post_id}'> View Post </a>or<a href='posts.php?source=add_post'> Add more posts</a></p>";
	}

?>

<form action="" method="post" enctype="multipart/form-data"><!-- Add Post form -->

	<div class="form-group">
		<label for="title">Post Title</label>
		<input class="form-control" type="text" name="title">
	</div>
	
	<h4>Author: <?php echo $_SESSION['firstname']." ".$_SESSION['lastname']; ?></h4><br>

	<div class="form-group"><!--..........Drop down categories selection..-->
		<label for="post_category">Category</label>
		<select name="post_category" id="">

		<?php  

				$query = "SELECT * FROM categories";
	    		$select_categories = mysqli_query($connection,$query);

	    		confirm($select_categories);

	    		while ($row = mysqli_fetch_assoc($select_categories)) {
	                $cat_id = $row['cat_id'];
	                $cat_title = $row['cat_title'];

	                echo "<option value='$cat_id'>{$cat_title}</option>";
	             
            	}

            ?>

		</select>
	</div><!--................Drop down categories.......-->	
	
	

	<div class="form-group">
		<label for="post_category">Status</label>
		<select name="post_status" id="">
			<?php echo "<option value='Draft'>Draft</option>"; ?>	
			<option value='Published'>Published</option>
		</select>
	</div>
	
	
	<div class="form-group">
		<label for="post_image">Post Image</label>
		<input type="file" name="image">
	</div>	
	
	<div class="form-group">
		<label for="post_tags">Post Tags</label>
		<input class="form-control" type="text" name="post_tags">
	</div>
	
	<div class="form-group">
		<label for="post_content">Post Content</label>
		<textarea class="form-control" name="post_content" id="body" cols="30" rows="10"></textarea>
	</div>

	<div class="form-group">
		<input class="btn btn-primary" type="submit" name="create_post" value="Publish Post">
	</div>

</form><!-- Add Category form --> 