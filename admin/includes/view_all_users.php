<table class="table table-condensed table-hover">
    <thead>
        <tr>
            <th>User ID</th>
            <th>Username</th>
            <th>Name</th>
            <th>Surname</th>
            <th>e-mail</th>
            <th>Role</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
    
    <?php 

    $query = "SELECT * FROM users";
    $select_users = mysqli_query($connection,$query);

    while ($row = mysqli_fetch_assoc($select_users)) {
        $user_id = $row['user_id'];
        $username = $row['username'];
        $user_password = $row['user_password'];
        $user_firstname = $row['user_firstname'];
        $user_lastname = $row['user_lastname'];    
        $user_email = $row['user_email'];
        $user_image = $row['user_image'];
        $user_role = $row['user_role'];
                      
       
        echo "<tr>";

        echo "<td>{$user_id}</td>";

        echo "<td>{$username}</td>";

        echo "<td>{$user_firstname}</td>";

        echo "<td>{$user_lastname}</td>";

        echo "<td><a>{$user_email}</a></td>";

        echo "<td>{$user_role}</td>";
                       
        echo "<td><a class='btn btn-sm btn-primary'href='users.php?source=edit_user&u_id={$user_id}'>Edit</a></td>";

    ?>

   <!-- **********  Delete Column.. -->
            <form method="post" action="">
                <input type="hidden" name="delete" value="<?php echo $user_id ?>" >
                <td><input class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Are you sure?!');" name="button" value="Delete"></td>
            </form>


<?php
        echo "</tr>";

    } ?>
           
            
            
  
    </tbody>
</table>


<?php

//delete Action

if(isset($_POST['delete'])){

    if(isset($_SESSION['role']) && $_SESSION['role'] == "Admin"){

        $the_user_id = mysqli_real_escape_string($connection, $_POST['delete']);

        $stmnt_0 = mysqli_prepare($connection,"DELETE FROM users WHERE user_id = ? ");
            
            mysqli_stmt_bind_param($stmnt_0,'i', $the_user_id);
            mysqli_stmt_execute($stmnt_0);
            mysqli_stmt_close($stmnt_0);

            header("Location: users.php");

    }
} 

?> 
