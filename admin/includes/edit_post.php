 <?php
 	if(isset($_GET['p_id'])){
 		$the_post_id = escape($_GET['p_id']);
 	}

 	$select_posts_by_id = mysqli_prepare($connection, "SELECT post_id, post_category_id, post_title, post_author, post_date, post_image, post_content, post_tags, post_comment_count, post_status FROM posts WHERE post_id = ?");

	mysqli_stmt_bind_param($select_posts_by_id,"i", $the_post_id);

    mysqli_stmt_execute($select_posts_by_id);

    mysqli_stmt_bind_result($select_posts_by_id, $post_id, $post_cat_id, $post_title, $post_author, $post_date, $post_image, $post_content, $post_tags, $post_comCount, $post_status);

	//load up table data to display in the iput boxes
   	mysqli_stmt_fetch($select_posts_by_id);
    $post_author_n = $_SESSION['firstname']." ".$_SESSION['lastname'];

	mysqli_stmt_close($select_posts_by_id);   



	// collect data to enter in the table on update
    if(isset($_POST['update_post'])){                       

    	$post_cat_id     = $_POST['post_category'];
        $post_title      = $_POST['title'];
        $post_author     = $_POST['author'];
        $post_image      = $_FILES['image']['name'];
        $post_image_temp = $_FILES['image']['tmp_name'];
        $post_content    = mysqli_real_escape_string($connection,$_POST['post_content']);
        $post_tags       = $_POST['post_tags'];
        $post_status     = $_POST['post_status']; 

        move_uploaded_file($post_image_temp, "../images/$post_image");

        if (empty($post_image)) {
        	$select_image = mysqli_prepare($connection, "SELECT post_image FROM posts WHERE post_id = ?");
        	mysqli_stmt_bind_param($select_image,"i",$the_post_id);
        	mysqli_stmt_execute($select_image);
        	mysqli_stmt_bind_result($select_image, $post_image);
        	mysqli_stmt_fetch($select_image);
        	mysqli_stmt_close($select_image);
        }
 			

 	// Update data in the table 

 		
 		
 		$update_post = mysqli_prepare($connection, "UPDATE posts SET post_title = ?, post_category_id = ?, post_date = now(), post_author = ?, post_status = ?, post_tags = ?, post_content = ?, post_image = ? WHERE post_id = ?"); 

 		mysqli_stmt_bind_param($update_post,"sisssssi", $post_title, $post_cat_id, $post_author, $post_status, $post_tags, $post_content, $post_image, $the_post_id);
 		mysqli_stmt_execute($update_post);
 		mysqli_stmt_close($update_post);

        confirm($update_post);

        echo "<p class='bg-success'>Post updated. <a href='../post.php?p_id={$the_post_id}'> View Post </a>or<a href='posts.php'> Edit more posts</a></p>";
	}
        
?>   


<form action="" method="post" enctype="multipart/form-data"><!-- Update Post form -->

	<div class="form-group">
		<label for="title">Post Title</label>
		<input class="form-control" type="text" name="title" value="<?php echo $post_title; ?>">
	</div>
	
	<!--- Cattegory -->
	<div class="form-group">
		<label for="post_category">Category</label>
		<select name="post_category" id="">

		<?php  

			//----- Default option ------------------------------------------------->

			$query = "SELECT * FROM categories WHERE cat_id = $post_cat_id";

			$collection = mysqli_query($connection,$query);

			$clean = mysqli_fetch_assoc($collection);

			$title = $clean['cat_title'];

		    echo "<option value='$post_cat_id'>{$title}</option>";

			//----- Options -------------------------------------------------------->

			$query = "SELECT * FROM categories";
    		$select_categories = mysqli_query($connection,$query);

    		confirm($select_categories);	    		
    		
    		while ($row = mysqli_fetch_assoc($select_categories)) {
                $cat_id = $row['cat_id'];
                $cat_title = $row['cat_title'];

                echo "<option value='$cat_id'>{$cat_title}</option>";	             
        	}

            ?>

		</select><!--- Cattegory -->
	</div>

	
	<div class="form-group">
		<label for="author">Post Author</label>
		<select name="author" id="">
			<?php echo "<option value='$post_author'>$post_author</option>"; ?>	
			<?php echo "<option value='$post_author_n'>$post_author_n</option>";?>	
			
	    </select>
	</div>	

	<div class="form-group">
		<label for="post_status">Status</label>
		<select name="post_status" id="">
			<?php echo "<option value='$post_status'>$post_status</option>"; ?>		
			<option value='Draft'>Draft</option>
			<option value='Published'>Published</option>
	    </select>
	</div>	
	
	<div class="form-group">
		<img src="../images/<?php echo $post_image; ?>" width='100' alt="">
		<input type="file" name="image">
	</div>	
	
	<div class="form-group">
		<label for="post_tags">Post Tags</label>
		<input class="form-control" type="text" name="post_tags" value="<?php echo $post_tags; ?>">
	</div>
	
	<div class="form-group">
		<label for="post_content">Post Content</label>
		<textarea class="form-control" name="post_content" id="body" cols="30" rows="10"><?php echo str_replace('\r\n', '</br>', $post_content); ?></textarea>
	</div>

	<div class="form-group">
		<input class="btn btn-primary" type="submit" name="update_post" value="Update All">
	</div>

</form><!-- Add Category form --> 