<div class="row">
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Data', 'Count'],

          <?php 

            $element_text = ['Active_post','Published  posts','Draft Posts','Comments', 'Comments Pen.','Cattegories','Users','Subscribers','Administrators'];
            $element_count = [$post_count, $post_published_count, $post_draft_count, $comment_count, $comment_draft_count, $category_count, $user_count, $user_sub_count, $user_admin_count ];
// update the counter $i<X  everytime you add or delete a field to display.!!
            for ($i = 0; $i < 9; $i++){

                echo "['{$element_text[$i]}'" . "," . "{$element_count[$i]}]," ;
            }

           ?>
          
                                  
        ]);

        var options = {
          chart: {
            title: '',
            subtitle: '',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>

    <div id="columnchart_material" style="width: 'auto'; height: 500px;"></div>

</div><!-- / Google graph -->