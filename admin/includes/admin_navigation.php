        <!-- Admin Navigation -->   
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="../index.php"><span class="glyphicon glyphicon-home"></span></a>
            </div>
            
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <!-- <li>
                    <a href=""><?php //echo users_online(); ?> users online</a>
                </li> -->

                <!-- <li>
                    <a href="">Users online<span class="usersonline"></span></a>
                </li> -->

                 
               
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img alt="photo" class="avatar float-left mr-1" src="../images/<?php echo photo() ?>?s=40&amp;v=4" height="20" width="20"> <?php echo $_SESSION['username']; ?><b class="caret"></b><span class="dropdown-caret"></span></a>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="profile.php"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        
                        <li>
                            <a href="message.php"><i class="fa fa-fw fa-file"></i>Edit message</a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="../includes/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul><!-- / Top Menu Items -->


           
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    
                    <li>
                        <a href="../admin/index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#posts_dropdown"><i class="fa fa-fw fa-file"></i> Post <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="posts_dropdown" class="collapse">
                            <li>
                                <a href="./posts.php?source=all_post"> View All Post</a>
                            </li>

                            <li>
                                <a href="posts.php?source=add_post"> Add Posts</a>
                            </li>
                        </ul>
                    </li>
                    
                    <?php 

                    if(is_admin($_SESSION['username'])){ ?>
                                              
                    <li>
                        <a href="./categories.php"><i class="fa fa-fw fa-file"></i> Categories</a>
                    </li>
                    
                    <li>
                        <a href="comments.php"><i class="fa fa-fw fa-file"></i> Comments</a>
                    </li>
                   
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-users"></i> Users <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="./users.php">View all Users</a>
                            </li>
                            <li>
                                <a href="./users.php?source=add_user">Add User</a>
                            </li>
                        </ul>
                    </li>
                    <?php } ?>
                  
                </ul>
            </div><!-- / Sidebar Menu Items -->
            
        </nav> <!-- / Admin Navigation -->