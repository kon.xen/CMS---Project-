<?php 

//********************************************************************************
// ** DB & SQL *******************************************************************
//********************************************************************************

// Registers a user / enters data to DB
function register_user($username,$email,$password){

	global $connection;

    $username      = escape($username);
    $password      = escape($password);
    $user_email    = escape($email);

    $user_password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
    
    $create_user   = mysqli_prepare($connection,"INSERT INTO users (username, user_password, user_email, user_role, user_image) VALUES (?, ?, ?, 'Subscriber', 'user-icon-placeholder.png')");

	mysqli_stmt_bind_param($create_user,"sss", $username, $user_password, $user_email);
    mysqli_stmt_execute($create_user);
    mysqli_stmt_close($create_user);

   
    // $query  = "INSERT INTO users (username, user_password, user_email, user_role,user_image) ";

    // $query .= " VALUES ('{$username}', '{$user_password}', '{$user_email}', 'Subscriber', 'user-icon-placeholder.png' )";

    // $create_user_query = mysqli_query($connection,$query);

    confirm($create_user);
        
}
//********************************************************************************


// Creates a new category / enters data to DB - KEEP !
function insert_categories(){

	global $connection;

	if (isset($_POST['add'])) {

		$cat_title = escape($_POST['cat_title']) ;

		if ($cat_title == "" || empty($cat_title)) {
			echo "this field should not be empty";
		} else {


			$ins_categories = mysqli_prepare($connection,"INSERT INTO categories(cat_title) VALUES (?)");

			mysqli_stmt_bind_param($ins_categories,"s",$cat_title);
            mysqli_stmt_execute($ins_categories);
            mysqli_stmt_close($ins_categories);

			confirm($ins_categories);

		}
	} 
}
function photo(){

	global $connection;

	$username = $_SESSION['username'];

	$get_photo = mysqli_prepare($connection, "SELECT user_image FROM users WHERE username = ?");
	mysqli_stmt_bind_param($get_photo,"s",$username);
	mysqli_stmt_bind_result($get_photo,$image);
	mysqli_stmt_execute($get_photo);
	mysqli_stmt_fetch($get_photo);
	mysqli_stmt_close($get_photo);
    
    return $image;

	// $result   = mysqli_query($connection,"SELECT user_image FROM users WHERE username = '$username'");

	// $image =  mysqli_fetch_array($result);

	// return $image['user_image'];
}
//********************************************************************************


// Retrieves and displays all categories in the CM - KEEP ! - Check
function findAllCategories(){

	global $connection;

	$all_categories = sellect_all('categories');

	// $select_categories = mysqli_query($connection,$query);

    while ($row = mysqli_fetch_assoc($all_categories)) {
    	$cat_id = $row['cat_id'];
        $cat_title = $row['cat_title'];
        echo "<tr>";
        echo "<td>{$cat_id}</td>";
        echo "<td>{$cat_title}</td>";
        echo "<td><a class='btn btn-sm btn-danger' href='categories.php?delete={$cat_id}'>Delete</a></td>";
        echo "<td><a class='btn btn-sm btn-primary' href='categories.php?edit={$cat_id}'>Edit</a></td>";
        echo "</tr>";
    }                                                         
}
//********************************************************************************



// Confirms a DB query... - KEEP !
function confirm($result){

	global $connection;

	if(!$result){
		die("QUERY FAILED ". mysqli_error($connection));
	}
}
//********************************************************************************


// Deletes objects / fields  | Generic -  KEEP !
function delete_object($table,$column,$parameter){

	global $connection;

	$delete = mysqli_prepare($connection,"DELETE FROM $table WHERE $column = ?");

	mysqli_stmt_bind_param($delete,"i",$parameter);
	mysqli_stmt_execute($delete);

	confirm($delete);

	mysqli_stmt_close($delete);
    }

//********************************************************************************


// Delets a cattegory specificaly - Keep !
function deleteCategory(){

	global $connection;

	    if (isset($_GET['delete'])){

	    	$del_cat_id = escape($_GET['delete']);
	    	delete_object('categories','cat_id',$del_cat_id);
	    	header(("location: categories.php"));
		}
}
//********************************************************************************


// Returns number of rows? in a table KEEP !
function item_count($table){

	global $connection;

		$items = mysqli_query($connection,"SELECT * FROM $table");
		return mysqli_num_rows($items);
}

//********************************************************************************

// Returns number of rows in a table related to parameter KEEP !
function item_count_comments($table,$column,$parameter){

	global $connection;
	$comment_count = 0;

	$items = mysqli_query($connection,"SELECT * FROM $table WHERE $column = $parameter");

	while ($row = mysqli_fetch_array($items)) {
		$number = $row['post_comment_count'];
		$comment_count = $comment_count + $number; 
	}

	return $comment_count;	

}
//********************************************************************************

// Returns number of rows in a column of table KEEP !
function item_count_plus($table,$column,$parameter){

    global $connection;
       
    $items = mysqli_query($connection,"SELECT * FROM $table WHERE $column = $parameter");
    
    return mysqli_num_rows($items);
    
}
//********************************************************************************


// Returns number of rows in a column of a table that match the search parameters 
// function item_count_plus_search($table,$column){

//        global $connection;
//        global $search;
       
//     $items = mysqli_query($connection,"SELECT * FROM $table WHERE $column LIKE '%$search%'");
    
//     return mysqli_num_rows($items);
    
// } 
//********************************************************************************


//
// function item_count_limited($table,$column,$parameter,$limit){

//        global $connection;
       
//     $items = mysqli_query($connection,"SELECT * FROM $table WHERE $column = $parameter LIMIT $limit,5") ;
    
//     return mysqli_num_rows($items);
    
// }
//********************************************************************************


// Selects all from table - Keep!
function sellect_all($table){

	global $connection;

	return mysqli_query($connection,"SELECT * FROM $table");
	
}
//********************************************************************************


// elects all from table that match parameter - Keep !
 function sellect_all_w($table, $column, $parameter){

 	global $connection;
	
	return mysqli_query($connection,"SELECT * FROM $table WHERE $column = $parameter ");
 } 
//********************************************************************************


// Updates the 'status' column in post table as per id ? -  Keep !!
function update_post_status($psvlid,$bulk){

	global $connection;
	
	$query = "UPDATE posts SET post_status = '{$bulk}' WHERE post_id = {$psvlid}";
	return mysqli_query($connection,$query);
 }
//******************************************************************************** 


// Updates the 'comment' column in post table as per id ? -  Keep !!
 function update_comment_status(){

	global $connection;

	$the_comment_id = escape($_GET['comid']);
	$status = escape($_GET['status']);

	$up_com_status = mysqli_prepare($connection, "UPDATE comments SET comment_status = ? WHERE comment_id = ?");
	mysqli_stmt_bind_param($up_com_status,"si",$status, $the_comment_id );
	
	mysqli_stmt_execute($up_com_status);
	
	mysqli_stmt_close($up_com_status);  


	// $query = "UPDATE comments SET comment_status = '{$status}' WHERE comment_id = $the_comment_id ";

	// return mysqli_query($connection,$query);
 } 
 //********************************************************************************

// Calculates the number of pages, also  posts / page to display -  Keep !!
 function pagination_calculation($from,$where,$option){	

 	global $connection;
 	global $origin;
 	global $count;
 	global $page;
 	global $page_1;

 	// Define how many posts to display per page...... using $page_1 & $count to send out values.
                    
    if (isset($_GET['page'])){

        $page = escape($_GET['page']);

    }  else {                        
        $page = "";
       }

    if ($page == "" || $page == 1){
        $page_1 = 0;
    } else {
        $page_1 = ($page * 5) - 5;

    }

    $count = item_count_plus($from,$where,$option);
    
    
     if ($page_1>$count) {
            $page_1 = $count;
        }

    $count = ceil($count / 5);

 }



//********************************************************************************
// ** Validations & Security *****************************************************
//********************************************************************************

// Logins a user based on $_SESSION data.
function login_user($username,$password){

	global $connection;

	$username = escape($username);
	$password = escape($password);

	$login = mysqli_prepare($connection, "SELECT user_id, user_password, username, user_firstname, user_lastname,user_role FROM users WHERE username = ? ");
	mysqli_stmt_bind_param($login,"s",$username);
	mysqli_stmt_bind_result($login, $db_id, $db_password, $db_username, $db_firstname, $db_lastname, $db_role);
	mysqli_stmt_execute($login);
	mysqli_stmt_fetch($login);
	mysqli_stmt_close($login);

	// $query ="SELECT * FROM users WHERE username = '{$username}'";
	// $select_user_query = mysqli_query($connection, $query);
	
	// confirm($select_user_query);

	// while ($row = mysqli_fetch_array($select_user_query)) {

	// 	$db_id        = $row['user_id'];
	// 	$db_password  = $row['user_password'];
	// 	$db_username  = $row['username'];
	// 	$db_firstname = $row['user_firstname'];
	// 	$db_lastname  = $row['user_lastname'];
	// 	$db_role      = $row['user_role'];
	// }

	//---validation--------------//

	if(password_verify($password, $db_password)){

		$_SESSION['username']  = $db_username;
		$_SESSION['firstname'] = $db_firstname;
		$_SESSION['lastname']  = $db_lastname;
		$_SESSION['role'] 	   = $db_role;

		redirect("/PHP/cms_project/admin/");

		} else {

		redirect("/PHP/cms_project/logon.php");
		
	}
}
//********************************************************************************


// Encrypts the user password
 //  Function PasswordEncryption($password){

 // 	global $connection;
 // 	global $safe_password;
 // 	// global $password;

	// $query = "SELECT randSalt FROM users";
 //    $select_randSalt_query = mysqli_query($connection, $query);

 //    confirm($select_randSalt_query);

 //    $row = mysqli_fetch_array($select_randSalt_query);
 //    $salt = $row['randSalt'];
 //    $safe_password = crypt($password, $salt);
 // }
//********************************************************************************


// Checks if the entered e-mail exists in the DB
function email_exists($email){

	global $connection;

	$query  = "SELECT user_email FROM users WHERE user_email = '$email'";
	$result = mysqli_query($connection,$query);

	if (mysqli_num_rows($result) > 0){

		return true;

	} else {

		return false;
	}
}
//********************************************************************************


// Checks if the user is an Admin return faulse / true - KEEP!
function is_admin($username){

	global $connection;

	$admin_is = mysqli_prepare($connection,"SELECT user_role FROM users WHERE username = ?");

	mysqli_stmt_bind_param($admin_is,"s",$username);
	mysqli_stmt_bind_result($admin_is, $usr_role);
	mysqli_stmt_execute($admin_is);
	mysqli_stmt_fetch($admin_is);
	mysqli_stmt_close($admin_is);

	// $query  = "SELECT user_role FROM users WHERE username = '$username'";
	// $result = mysqli_query($connection,$query);

	// $row    = mysqli_fetch_array($result);

	// if ($row['user_role'] =='Admin'){

	if ($usr_role == 'Admin'){

		return true;

	} else {

		return false;
	}
}
//********************************************************************************


// Checks if the users name exists in the DB, return faulse || true - KEEP!
function username_exists($username){

	global $connection;

	
	$query  = "SELECT username FROM users WHERE username = '$username'";
	$result = mysqli_query($connection,$query);

	if (mysqli_num_rows($result) > 0){

	
		return true;

	} else {

		return false;
	}
}
//********************************************************************************


// cleans up $_POST - Keep !!
function clean_Post(){

	global $connection;   

	foreach ($_POST as $key => $value) {
                $clean[$key] = escape($value);
    }

    return $clean;

}
//********************************************************************************


// use Before entering data in a DB to clean up the data...
function escape($string){
	global $connection;

	return mysqli_real_escape_string($connection, trim($string));
}
//********************************************************************************


//********************************************************************************
// Navigation ********************************************************************
//********************************************************************************


function redirect($location){
	return header("Location:" . $location);
}
//********************************************************************************



//***************
//* coments    	****
//* tag   ?    	*******
//*******************************************************************************



// function users_online(){

// 	if(isset($_GET['onlineusers'])){

// 		global $connection;
	
// 		if(!$connection){ 

// 			session_start();

// 			include  ("./../../includes/db.php"); 

// 			$session = session_id();
// 		    $time=time();
// 		    $time_out_seconds = 60;
// 		    $time_out = $time - $time_out_seconds;

// 		    $session_query = "SELECT * FROM users_online WHERE session = '$session'";
// 		    $send_query = mysqli_query($connection, $session_query);
// 		    $count = mysqli_num_rows($send_query);

// 		    if ($count == NULL) {
		    
// 		        mysqli_query($connection,"INSERT INTO users_online(session,time) VALUES('$session', '$time')");
		    
// 			} else {

// 			    mysqli_query($connection,"UPDATE users_online SET time = '$time' WHERE session = '$session'"); 
// 		    }

		    
// 		     $users_online_query = mysqli_query($connection, "SELECT * FROM users_online WHERE time > '$time_out'");
		     
// 		     confirm($users_online_query);

// 		     echo $count_user = mysqli_num_rows($users_online_query);
// 		}
// 	}
// }
// users_online();
                                        
?>


