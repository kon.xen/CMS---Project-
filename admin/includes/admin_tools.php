<!--  Admin tools Mechanics -->
 <?php 
 
    if (isset($_POST['checkBoxArray'])){
        
        foreach ($_POST['checkBoxArray'] as $postValueId) {
            $bulk_options = escape($_POST['bulk_options']);


            switch($bulk_options){

                case 'Published':                  
                   $update_to_published_status = update_post_status($postValueId, $bulk_options);           
                break;

                case 'Draft':                    
                   $update_to_draft_status = update_post_status($postValueId, $bulk_options);               
                break;

                case 'clone':  

                // Gather all data ... 
                    
                    $select_post = mysqli_prepare($connection,"SELECT post_category_id, post_title, post_author, post_date, post_image, post_content, post_tags, post_status FROM posts WHERE post_id = ?");

                    mysqli_stmt_bind_param($select_post,"i",$postValueId);
                    mysqli_stmt_execute($select_post);
                    mysqli_stmt_bind_result($select_post, $post_cat_id, $post_title, $post_author, $post_date, $post_image, $post_content, $post_tags, $post_status);
                                      
                    confirm($select_post);

                    mysqli_stmt_fetch($select_post);

                    mysqli_stmt_close($select_post);

                //... now make the clone !!

                    $query = "INSERT INTO posts(post_category_id, post_title, post_author, post_date, post_image, post_content,  post_tags, post_status ) ";
                    $query .= " VALUES ({$post_cat_id}, '{$post_title}', '{$post_author}', '{$post_date}', '{$post_image}', '{$post_content}',  '{$post_tags}', '{$post_status}')";

                    $copy_query = mysqli_query($connection, $query);
                   
                    confirm($copy_query);
                break;
                
                case 'reset':
                    $reset_views = mysqli_prepare($connection, "UPDATE posts SET post_views_counts = 0 WHERE post_id = ?");
                    mysqli_stmt_bind_param($reset_views,"i",$postValueId); 
                    mysqli_stmt_execute($reset_views);
                    
                    mysqli_stmt_close($reset_views);
                   
                    confirm($reset_views); //for development to production realy...
                break;

                case 'delete':
                    delete_object('posts','post_id',$postValueId); 
                    delete_object('comments','comment_post_id',$the_post_id);        
                    header(("location: posts.php"));
                break;
               
            }
        }
    }

?>