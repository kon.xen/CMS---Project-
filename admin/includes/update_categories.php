<form action="" method="post">
	<div class="form-group">
		<label for="cat_title">Update Category</label>

		<?php 

		if (isset($_GET['edit'])){
			$edit_cat_id = escape($_GET['edit']);

            $stmnt1 = mysqli_prepare($connection,"SELECT * FROM categories WHERE cat_id = ?");
            
            mysqli_stmt_bind_param($stmnt1,'i', $cat_id);
            mysqli_stmt_execute($stmnt1);
            mysqli_stmt_bind_result($stmnt1,$cat_id, $cat_title);
            mysqli_stmt_close($stmnt1);
    	}

        ?> 

        <input value="<?php if (isset($cat_title)){ echo $cat_title;} ?>" class="form-control" type="text" name="cat_title">

		
        <?php // UPDATE query action

        if (isset($_POST['update_category'])){
            $the_cat_title = $_POST['cat_title'];

            $stmnt2 = mysqli_prepare($connection,"UPDATE categories SET cat_title = ? WHERE cat_id = ? ");
            
            mysqli_stmt_bind_param($stmnt2,'si', $the_cat_title, $cat_id);
            mysqli_stmt_execute($stmnt2);
            mysqli_stmt_close($stmnt2);


            if (!$stmnt1){
                die("QUERY FAILED". mysqli_error($connection));
            }

            redirect("categories.php");
        }

        ?>                      			

		
	</div>
	<div class="form-group">
		<input class="btn btn-primary" type="submit" name="update_category" value="Update">
	</div>
</form>

