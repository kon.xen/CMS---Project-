<?php 

	if(isset($_POST['create_user'])){
		
		$username = escape($_POST['username']);
        $user_password = $_POST['user_password'];
        $user_firstname = escape($_POST['user_firstname']);
        $user_lastname = escape($_POST['user_lastname']);
        $user_email = escape($_POST['user_email']);
        $user_image = $_FILES['image']['name'];
        $user_image_temp = $_FILES['image']['tmp_name'];
        $user_role = $_POST['user_role'];
        			
				
		move_uploaded_file($user_image_temp, "../images/$user_image");

		$user_password = password_hash($user_password, PASSWORD_BCRYPT, array('cost' => 12));


		$create_user = mysqli_prepare($connection, "INSERT INTO users (username, user_password, user_firstname, user_lastname, user_email, user_image, user_role) VALUES (?, ?, ?, ?, ?, ?, ?)");
            
        mysqli_stmt_bind_param($create_user, "sssssss", $username, $user_password, $user_firstname, $user_lastname, $user_email, $user_image, $user_role);
        mysqli_stmt_execute($create_user);
        mysqli_stmt_close($create_user);

		echo "user Created: " . " " . "<a href='users.php' class='nav-link'>View Users</a>";
	}


?>


 <!-- Add user form -->
<form action="" method="post" enctype="multipart/form-data">

	<div class="form-group">
		<label for="image">User Image</label>
		<input type="file" name="image">
	</div>	

	<div class="form-group">
		<label for="user_firstname">Name</label>
		<input class="form-control" type="text" name="user_firstname">
	</div>

	<div class="form-group">
		<label for="user_lastname">Surname</label>
		<input class="form-control" type="text" name="user_lastname">
	</div>

	<div class="form-group">
		<label for="username">Username</label>
		<input class="form-control" type="text" name="username">
	</div>
	
	<div class="form-group">
		<label for="user_password">Password</label>
		<input class="form-control" type="password" name="user_password">
	</div>

	<div class="form-group">
		<select name="user_role" id="">
			<option value='subscriber'>User type</option>
			<option value='admin'>Admin</option>
			<option value='subscriber'>Subscriber</option>
		</select>
	</div>

	<div class="form-group">
		<label for="user_email">e-mail</label>
		<input class="form-control" type ="email" name="user_email" >
	</div>
	
	<div class="form-group">
		<input class="btn btn-primary" type="submit" name="create_user" value="Add User">
	</div>

</form><!-- / Add user form --> 