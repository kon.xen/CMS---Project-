<table class="table table-condensed table-hover">
    <thead>
        <tr>
            <th>Author</th>
            <th>Comment</th>
            <th>e-mail</th>
            <th>Status</th>
            <th>Post</th>
            <th>Date</th>
            <th>Approve</th>
            <th>Disapprove</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody class="">
    
    <?php 

    $select_comments = sellect_all('comments');

    while ($row = mysqli_fetch_assoc($select_comments)) {
        $comment_id      = $row['comment_id'];
        $comment_post_id = $row['comment_post_id'];
        $comment_author  = $row['comment_author'];
        $comment_email   = $row['comment_email'];
        $comment_content = $row['comment_content'];
        $comment_status  = $row['comment_status'];    
        $comment_date    = $row['comment_date'];               
       
        echo "<tr>";

        echo "<td>{$comment_author}</td>";

        echo "<td class='small col-sm-1'>{$comment_content}</td>";

        echo "<td>{$comment_author}</td>";

        echo "<td>{$comment_status}</td>";

        $select_post = mysqli_prepare($connection,"SELECT post_id, post_title FROM posts WHERE post_id = ?");
        mysqli_stmt_bind_param($select_post,"i",$comment_post_id);
        mysqli_stmt_execute($select_post);
        mysqli_stmt_bind_result($select_post, $post_id, $post_title);

        confirm($select_post);

               

       while (mysqli_stmt_fetch($select_post)){ 
      
        echo "<td><a href='../post.php?p_id=$post_id'>$post_title</a></td>";
      
        } 

        echo "<td>{$comment_date}</td>";

        echo "<td><a class='btn btn-sm btn-success' href='comments.php?comid=$comment_id&status=approved'>Approve</a></td>";

        echo "<td><a class='btn btn-sm btn-warning' href='comments.php?comid=$comment_id&status=disapproved'>Disapprove</a></td>";

        echo "<td><a class='btn btn-sm btn-danger' href='comments.php?delete=$comment_id'>Delete</a></td>";

        echo "</tr>";

        mysqli_stmt_close($select_post);

    }

    ?>
  
    </tbody>
</table>


<!-- Delete and Update -->
<?php 

//delete Action
if(isset($_GET['delete'])){ 
    $the_comment_id = escape($_GET['delete']);

    delete_object('comments','comment_id', $the_comment_id);
    
    //reduce by 1 the number of comment counts.
    $stmnt3 = mysqli_prepare($connection,"UPDATE posts SET post_comment_count = post_comment_count - 1 WHERE post_id = ?");
    mysqli_stmt_bind_param($stmnt3,"i",$comment_post_id );
    mysqli_stmt_execute($stmnt3);
    mysqli_stmt_close($stmnt3);
    
    header("Location: comments.php");
}

 // Update Action
if(isset($_GET['comid'])){
   update_comment_status();

   header("Location: comments.php");
}
 
?>
