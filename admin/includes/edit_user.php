 <?php

 	if(isset($_GET['u_id'])){

 		$the_user_id = escape($_GET['u_id']);

 		$stmnt_users = mysqli_prepare($connection,"SELECT user_id, username, user_password, user_firstname, user_lastname, user_email, user_image, user_role FROM users WHERE user_id = ? ");
 		
 		mysqli_stmt_bind_param($stmnt_users, 'i', $the_user_id);
        mysqli_stmt_execute($stmnt_users);
        mysqli_stmt_bind_result($stmnt_users,$user_id, $username, $user_password, $user_firstname, $user_lastname, $user_email, $user_image, $user_role);
        

        mysqli_stmt_fetch($stmnt_users);

        mysqli_stmt_close($stmnt_users);
 	

	    // $query = "SELECT * FROM users WHERE user_id = $the_user_id ";
	    // $select_users_by_id = mysqli_query($connection,$query);

	    
//load up table data to display in the iput boxes

	    // while ($row = mysqli_fetch_assoc($select_users_by_id)) { 

	        // $user_id        = $row['user_id'];
	        // $username       = $row['username'];
	        // $user_password  = $row['user_password'];
	        // $user_firstname = $row['user_firstname'];
	        // $user_lastname  = $row['user_lastname'];
	        // $user_email     = $row['user_email'];
	        // $user_image     = $row['user_image'];
	        // $user_role      = $row['user_role'];
	    //}    

// collect data to enter in the table on update

	    if(isset($_POST['update_user'])){

	    	$username        = $_POST['username'];
	        $user_password   = $_POST['user_password'];
	        $user_firstname  = $_POST['user_firstname'];
	        $user_lastname   = $_POST['user_lastname'];
	        $user_email      = $_POST['user_email'];    
	        $user_image      = $_FILES['image']['name'];
	        $user_image_temp = $_FILES['image']['tmp_name'];
	        $user_role       = $_POST['user_role'];

	        move_uploaded_file($user_image_temp, "../images/$user_image");

	        if (empty($user_image)) {
	        	$query = "SELECT * FROM users WHERE user_id = $the_user_id";
	        	$select_image = mysqli_query($connection,$query);

				while($row = mysqli_fetch_array($select_image)){
					$user_image = $row['user_image'];
				}

// password encryption               

	        if (!empty($user_password)){

	        	$get_user = mysqli_prepare($connection,"SELECT user_password FROM users WHERE user_id = ?");
	        	mysqli_stmt_bind_param($get_user,'i', $the_user_id);
	        	mysqli_stmt_execute($get_user);
        		mysqli_stmt_bind_result($get_user, $db_user_password);

	        	mysqli_stmt_fetch($get_user);
	        	

	           	if ($db_user_password != $user_password){

	        	$safe_password = password_hash($user_password, PASSWORD_BCRYPT, array('cost' => 12));
	        	mysqli_stmt_close($get_user);
	        	
	        	} else {

	        		$safe_password = password_hash($user_password, PASSWORD_BCRYPT, array('cost' => 12));
	        		mysqli_stmt_close($get_user);
	        	}

// Updated data in the table 

	       	$update = mysqli_prepare($connection, "UPDATE users SET username = ?, user_password  = ?, user_firstname = ?, user_lastname  = ?, user_email = ?, user_image = ?, user_role = ? WHERE user_id = ? ");
            
            mysqli_stmt_bind_param($update,'sssssssi', $username, $safe_password, $user_firstname, $user_lastname, $user_email,$user_image, $user_role, $the_user_id);

            mysqli_stmt_execute($update);

          	mysqli_stmt_close($update);

			// $query  = "UPDATE users SET ";
	  //       $query .="username       = '{$username}', ";
	  //       $query .="user_password  = '{$safe_password}', ";
	  //       $query .="user_firstname = '{$user_firstname}', ";
	  //       $query .="user_lastname  = '{$user_lastname}', ";
	  //       $query .="user_email     = '{$user_email}', ";
	  //       $query .="user_image     = '{$user_image}', ";
	  //       $query .="user_role      = '{$user_role}' ";
	  //       $query .="WHERE user_id  = '{$the_user_id}' ";
	        

	  //       $update_user = mysqli_query($connection,$query);

	  //       confirm($update_user);

	        }
	    	}	
	 	} 
	} else {

	 		header("location: index.php");
	}


?>   


<!-- Update User form -->
<form action="" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label for="username">username</label>
		<input class="form-control" type="text" name="username" value="<?php echo $username; ?>">
	</div>
	
	<div class="form-group">
		<label for="password">Password</label>
		<input class="form-control" type="password" name="user_password" value="<?php echo $user_password; ?>">
	</div>

	<div class="form-group">
		<label for="first_name">Name</label>
		<input class="form-control" type="text" name="user_firstname" value="<?php echo $user_firstname; ?>">
	</div>

	<div class="form-group">
		<label for="last_name">Surname</label>
		<input class="form-control" type="text" name="user_lastname" value="<?php echo $user_lastname; ?>">
	</div>

	<div class="form-group">
		<label for="email">e-mail</label>
		<input class="form-control" type="email" name="user_email" value="<?php echo $user_email; ?>">
	</div>
	
	<div class="form-group">
		<img src="../images/<?php echo $user_image; ?>" width='100' alt="">
		<input type="file" name="image">
	</div>	
	
	<div class="form-group">
		<select name="user_role" id="">
			<?php echo "<option value='$user_role'>$user_role</option>"; ?>			
			<option value='Admin'>Admin</option>
			<option value='Subscriber'>Subscriber</option>
			<option value='Guest'>Guest</option>
	    </select>
	</div>

	<div class="form-group">
		<input class="btn btn-warning" type="submit" name="update_user" value="Update User">
	</div>

</form><!-- / Update User form  --> 