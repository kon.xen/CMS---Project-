<?php include "includes/admin_header.php"; ?>

<?php if(!is_admin($_SESSION['username'])){

    header("location: index.php");
}
?>


<div id="wrapper">

<?php include "includes/admin_navigation.php"; ?>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Categories | <small><?php echo $_SESSION['username']; ?></small></h3><!-- Page Heading -->

                    <div class="col-xs-6">

                    <?php insert_categories(); ?>

						<!-- Add Category form -->
                    	<form action="" method="post">
                    		<div class="form-group">
                    			<label for="cat_title">Add Category</label>
                    			<input class="form-control" type="text" name="cat_title">
                    		</div>
                    		<div class="form-group">
                    			<input class="btn btn-primary" type="submit" name="add" value="Add Category">
                    		</div>
                    	</form><!-- / Add Category form --> 


                        <!--  update category action -->
                        <?php 

                        if(isset($_GET['edit'])){
                            $cat_id = escape($_GET['edit']);

                            include "includes/update_categories.php";
                        }

                        ?><!--  / update category action -->

                    </div>


                    <div class="col-xs-6"><!-- Category list -->
                    
                        <?php $select_categories = sellect_all('categories'); ?>

                        <table class="table table-hover">
                        	<thead>
                        		<tr>
                        			<th>ID</th>
                        			<th>Category title</th>
                                    <th></th>
                                    <th></th> 
                        		</tr>
                                
                        	</thead>

                        	<tbody>
                                <?php

                                findAllCategories();

                                deleteCategory(); 

                                ?> 
                            </tbody>
                         </table>

                    </div><!-- / Category list -->       

                </div><!-- / Column -->

            </div><!-- / Row -->                

        </div> <!-- / container-fluid -->

        <?php include "../includes/footer.php"; ?>     

    </div><!-- / Page wraper-->

</div><!-- / Wraper-->


