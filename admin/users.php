<?php include "includes/admin_header.php"; ?>

<?php if(!is_admin($_SESSION['username'])){

    header("location: index.php");
}


 ?>

    <div id="wrapper">
        
    <!-- Navigation -->    
    <?php include "includes/admin_navigation.php"; ?>

        <div id="page-wrapper">

            <div class="container-fluid">
                
                <div class="row"><!-- Page Heading -->
                    
                    <div class="col-lg-12">
                        <h3 class="page-header">Users | <small><?php echo $_SESSION['username']; ?></small></h3>

                        <!-- Loader -->
                        <?php 

                        if(isset($_GET['source'])){
                                $source = escape($_GET['source']);
                        } else{
                            $source ='';
                        }

                        switch ($source) {
                            case 'add_user':
                                include "includes/add_user.php";
                                break;
                            case 'edit_user':
                                include "includes/edit_user.php";
                                break;
                            case '200':
                                echo "nice200";
                                break;

                            default:
                                include "includes/view_all_users.php";
                                break;
                        }
                        ?><!-- / Loader -->
                        
                    </div><!--  / --> 
                        
                </div><!-- /.row -->        

            </div><!-- /.container-fluid -->

            <?php include "../includes/footer.php"; ?>          

        </div><!-- / Page Wraper -->

</div><!-- / Wraper -->