            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">
                            ADMIN for
                            <small><?php echo $_SESSION['username']; ?></small>
                        </h3>                          
                    </div>
                </div><!-- / Page Heading -->

                    
                <!-- Counts for graph -->
                <?php 

                    $post_published_count = item_count_plus("posts", "post_status", "'published'");
                   
                    $post_draft_count     = item_count_plus("posts", "post_status", "'draft'");
                    
                    $comment_draft_count  = item_count_plus("comments", "comment_status", "'Dissapproved'");
                    
                    $user_sub_count       = item_count_plus("users", "user_role", "'Subscriber'");
                                                    
                    $user_admin_count     = item_count_plus("users", "user_role", "'Admin'");
                
                 ?><!-- / Counts for graph -->

                <!-- Widgets -->
                <?php include "includes/admin_widget_top.php"; ?>

                
                <!-- Google graph -->
                <?php include "includes/admin_graph.php"; ?>                
                

            </div><!-- / Container-fluid -->

            
        