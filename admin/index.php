<?php include "includes/admin_header.php"; ?>

    <div id="wrapper">
        
        <div id="page-wrapper">
        
    <!-- Navigation -->    
    <?php include "includes/admin_navigation.php"; ?>

    <?php 
    
        if($_SESSION['role'] == 'Admin'){

            include "admin_index.php";
        
        } else {

            include "user_index.php";
            }

    ?>      

            
            <?php include "../includes/footer.php"; ?>

        </div><!-- / Page-Wrapper -->

    </div><!--/ Wrapper -->     