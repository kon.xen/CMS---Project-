<?php include "includes/db.php" ?>
<?php include "includes/header.php" ?>


    <!-- Navigation -->
<?php include "includes/navigation.php";?>
   
<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <!-- Retrieve & display info -->
        <div class="col-md-8">
            
            <?php 
                            
                if(isset($_GET['p_id'])){
                    $the_post_id     = escape($_GET['p_id']);
                    $the_post_author = escape($_GET['author']);
                }         
                
                $stmnt0 = mysqli_prepare($connection,"SELECT post_id, post_title, post_author, post_date, post_image, post_content FROM posts WHERE post_author = ? ");
            
                mysqli_stmt_bind_param($stmnt0,'s', $the_post_author);
                mysqli_stmt_execute($stmnt0);
                mysqli_stmt_bind_result($stmnt0, $post_id, $post_title, $post_author, $post_date, $post_image, $post_content);

                //mysqli_stmt_close($stmnt0);

                while (mysqli_stmt_fetch($stmnt0)){

                $post_content = substr($post_content,0,200);

                // !!query before prepareed statement!!!.
                // $query = "SELECT * FROM posts WHERE post_author = '{$the_post_author}'";
                // $select_all_posts_query = mysqli_query($connection,$query);
                             
                // while ($row = mysqli_fetch_assoc($select_all_posts_query)) {
                //     $post_id      = $row['post_id'];
                //     $post_title   = $row['post_title'];
                //     $post_author  = $row['post_author'];
                //     $post_date    = $row['post_date'];
                //     $post_image   = $row['post_image'];
                //     $post_content = substr($row['post_content'],0,200);
                // !!
                                                  
            include "blogpost.php";
            //<!-- Blog Post layout -->
            

             } 
             mysqli_stmt_close($stmnt0);

             ?>               

        </div> <!--  / Retrieve & display info -->

        

        <!-- Blog Sidebar Widgets Column -->
        <?php include "includes/sidebar.php" ?>


    </div><!-- /.row -->


</div><!-- Page Content -->
        
       
<?php include "includes/footer.php"; ?>