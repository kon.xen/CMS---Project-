<?php ob_start(); ?>
<?php  include "includes/db.php"; ?>
<?php "../admin/functions.php" ?>
<?php  include "includes/header.php"; ?>
cc

<!-- Page Content -->
<div id="page-wrapper">

<!-- Navigation -->
<?php include "includes/navigation.php"; ?>

<div class="container-fluid">
    <section id="login">
        <div class="container">

            <!--  -->
            <div class="row">
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="form-wrap">
                    
                    <h1>Login</h1>

                        <form role="form" action="includes/login.php" method="post" id="login-form" autocomplete="off">

                            <div class="form-group">
                                <label for="username" class="sr-only">username</label>
                                <input type="text" name="username" id="username" class="form-control" placeholder="Enter Desired Username" autocomplete="on" value="<?php echo isset($username
                                    ) ? $username : '' ?>">
                                <p class=""><?php echo isset($error['username']) ? $error['username'] : '' ?></p>
                            </div>
                             
                             <div class="form-group">
                                <label for="password" class="sr-only">Password</label>
                                <input type="password" name="password" id="key" class="form-control" placeholder="Password">
                                <p class=""><?php echo isset($error['password']) ? $error['password'] : '' ?></p>
                            </div>
                    
                            <input type="submit" name="login" id="btn-login" class="btn btn-custom btn-lg btn-block btn-info" value="Login">
                        </form>

                        <br>
                        <br>
                        
                        <!-- register - redirect --> 
                        <div >
                            <ul class="list-unstyled list-inline">
                            <li><p>Not registered? create a free acount here</small></li>
                            <li><a href="registration.php"><button type="button" class="btn btn-xs btn-default">Register</button></a></li>
                            </ul>
                        </div>
                     
                    </div>
                </div> <!-- /.col-xs-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section>

</div><!-- / page content - Container -->
<div class="container-fluid"></div>

<?php include "includes/footer.php";?>

</div><!-- /#page-wrapper -->

