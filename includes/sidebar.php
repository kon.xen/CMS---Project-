<div class="col-md-4">
     <!-- Side Widget Well -->
   <?php include "widget.php"; ?>


    <!-- Login -->
    <div class="well">

        <?php if(isset($_SESSION["role"])): ?>
            <h5>Logged in as: <?php echo$_SESSION['username']; ?></h5>
            <a href="includes/logout.php" class="btn btn-xs btn-primary">Logout</a>

        <?php else: ?>

            <h4>Login</h4>
            <form action="includes/login.php" method="post">
                
                <div class="form-group">
                    <input name="username" type="text" class="form-control" placeholder="Enter username">
                </div>

                <div class="input-group">
                    <input name="password" type="password" class="form-control" placeholder="Enter Password">
                    <span class="input-group-btn">
                         <button class="btn btn-primary" type="submit" name="login" >Submit</button>
                    </span>
                </div>                           
            </form>       
                   

        <?php endif; ?>
        
    </div><!-- / Login -->
    

    <!-- Blog Search -->
    <div class="well">
        <h4>Blog Search</h4>
        <form action="search.php" method="post"  >
            <div class="input-group">
                <input name="search" type="text" class="form-control">
                <span class="input-group-btn"> 
                    <button name="submit" class="btn btn-default" type="submit">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.input-group --> 
    </div><!-- / Blog Search -->
                                         

    <!-- Blog Categories -->
    <div class="well">

    <?php 

    $sidebar_categories = sellect_all('categories');
    
    ?>


        <h4>Blog Categories</h4>
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-unstyled">
                    <?php 
                        while ($row = mysqli_fetch_assoc($sidebar_categories)) {
                            $cat_title = $row['cat_title'];
                            $cat_id = $row['cat_id'];
                            echo "<li><a href='category.php?category=$cat_id'>{$cat_title}</a></li>";
                        }
                    ?>
                </ul>
            </div><!-- /.col-lg-12 -->
         
        </div><!-- /.row -->
        
    </div><!-- / Blog Categories -->

</div>