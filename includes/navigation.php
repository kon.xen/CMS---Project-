 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-home"></span></a>
            </div>
            
            <ul class="nav navbar-right top-nav">
                
            <?php if(isset($_SESSION['role'])){ ?>
                 <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img alt="photo" class="avatar float-left mr-1" src="images/<?php echo photo() ?>?s=40&amp;v=4" height="20" width="20"> <?php echo $_SESSION['username']; ?><b class="caret"></b><span class="dropdown-caret"></span></a>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="admin/index.php"><i class="fa fa-fw fa-dashboard"></i> Admin area</a>
                        </li>

                        <li>
                            <a href="admin/profile.php"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        
                        <li class="divider"></li>

                        <li>
                            <a href="includes/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
                
              <?php } ?> 

            </ul>

            <!-- Top Navigation Links-->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">

                    <!-- Collect the nav links, forms, and other content for toggling  -->
                    <?php
                    
                    $navigation_categories = sellect_all('categories');

                    while ($row = mysqli_fetch_assoc($navigation_categories)) {
                        $cat_title = $row['cat_title'];
                        $cat_id = $row['cat_id'];
                        echo "<li><a href='category.php?category=$cat_id'>{$cat_title}</a></li>";
                    
                    } ?>
                    
                    
                    <li>
                        <a href="contact.php">Contact</a>
                    </li>

                   
                    <?php if(!isset($_SESSION['role'])): ?>
                        <li><a href="logon.php">Login</a></li>

                    <?php endif; ?>  


                    <?php 
                   
                    if (isset($_SESSION['role'])) {

                        if (isset($_GET['p_id'])) {

                            $the_post_id = escape($_GET['p_id']);

                            echo "<li><a href='admin/posts.php?source=edit_post&p_id={$the_post_id}'> Edit Post </a></li>";
                        }
                    }
                    
                    ?>
              
                </ul>  
            </div><!--  / Top Navigation Links-->           
                     
    </div><!-- / container -->
       
</nav>
