</div>

<footer id="main-footer">

    <div class="container">

        <section class="row">       

                <!-- About Us -->
                <div class="col-md-3">
                    <div class="inner grey">
                        <h5>About us</h5>
                        <ul class="list-unstyled">
                            <li><a href="#">Profile</a></li>
                           <!--  <li><a href="#">Governance</a></li>
                            <li><a href="#">Disclaimer</a></li> -->
                            <li><a href="contact.php">Contact</a></li>
                        </ul>                        
                    </div>
                </div><!-- / About Us -->

                <!-- Credits -->
                <div class="col-md-3">
                    <div class="inner grey">
                        <h5>Credits</h5>
                        <ul class="list-unstyled"> 
                            <li><a href="http://glyphicons.com/" target="_blank">Glyphicons used under CC license</a></li>
                            <li><a href="http://codingfaculty.com/" target="_blank">Coding faculty</a></li>
                            <!-- <li><a href="#">Something else</a></li> -->
                        </ul>
                    </div>
                </div><!-- / Credits -->

                <!-- Follow us & social Links -->
                <div class="col-md-3">
                    <div class="inner grey">
                        <h5>Follow us</h5>
                        <ul class="social-links list-inline">
                            <li><a href="https://github.com/Kon-Xen" target="_blank"><i class="fa fa-github-square"></i></a></li>

                            <li><a href="https://linkedin.com/in/kxdesign" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
                        </ul>
                    </div>
                </div><!-- / Follow us & social Links -->

        </section><!-- / Row -->

    </div><!-- / Container -->
            
        <!-- copyright at bottom-->
        <div class="copy text-center grey"><p>© KX 2018 - <a href="#">PRIVACY</a></p>
        </div><!-- /copyright at bottom-->

</footer>

<!--Scripts-->

<!-- jQuery -->
<script src="js/jquery.js"></script>
<script src="js/scripts.js"></script>


<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

    
</body>

</html>
